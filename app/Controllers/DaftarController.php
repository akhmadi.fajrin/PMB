<?php

namespace App\Controllers;

use App\Models\DaftarModel;

class DaftarController extends BaseController
{
    protected $daftarModel;

    public function __construct()
    {
        $this->daftarModel = new DaftarModel();
        helper('form');
    }

    public function index()
    {
        // $data = [
        //     'title' => 'CRUD Object Model',
        //     'all_data' => $this->daftarModel->findAll()
        // ];

        // return view('objects/index', $data);
    }

    public function pilihjalur()
    {
        $data = [
            'bodyclass' => 'fullscreen-bg h-auto fbg-v2'

        ];
        echo view('layout/header', $data);
        echo view('jalur_penerimaan');
        echo view('layout/footer');
    }
    public function pilihprodi()
    {
        $data = [
            'bodyclass' => 'fullscreen-bg fbg-v2'

        ];
        echo view('layout/header', $data);
        echo view('data_prodi');
        echo view('layout/footer');
    }
    public function savepilihprodi()
    {
        // $data['title'] = 'Add Data';
        $data = [
            'bodyclass' => 'fullscreen-bg fbg-v2'
        ];
        if ($this->request->getPost()) {
            $rules = [
                'sistemkuliah' => 'required',
                'pilihan1' => 'required',
                'pilihan2' => 'required'
            ];

            if ($this->validate($rules)) {

                $inserted = [
                    'sistemkuliah' => $this->request->getPost('sistemkuliah'),
                    'pilihan1' => $this->request->getPost('pilihan1'),
                    'pilihan2' => $this->request->getPost('pilihan2')
                ];

                $this->daftarModel->insert($inserted);
                session()->setFlashData('success', 'data has been added to database');
                return redirect()->to('/datainputsatu');
            } else {
                session()->setFlashData('failed', \Config\Services::validation()->getErrors());
                return redirect()->back()->withInput();
            }
        }
        // return view('objects/form_add', $data);        
        echo view('layout/header', $data);
        echo view('daftar', $data);
        echo view('layout/footer');
    }

    public function datainputsatu()
    {
        $data = [
            'bodyclass' => 'fullscreen-bg fbg-v2'
        ];
        echo view('layout/header', $data);
        echo view('daftar', $data);
        echo view('layout/footer');
    }
    public function savedatainputsatu()
    {
        $data['title'] = 'Add Data';
        if ($this->request->getPost()) {
            $rules = [
                'nama' => 'required',
                'nomorktp' => 'required|max_length[16]|numeric',
                'nisn' => 'required|max_length[10]|numeric',
                'hp' => 'required|numeric'

            ];

            if ($this->validate($rules)) {

                $inserted = [
                    'nama' => $this->request->getPost('nama'),
                    'nomorktp' => $this->request->getPost('nomorktp'),
                    'nisn' => $this->request->getPost('nisn'),
                    'hp' => $this->request->getPost('hp')
                ];

                $this->daftarModel->insert($inserted);
                session()->setFlashData('success', 'data has been added to database');
                return redirect()->to('/datainput');
            } else {
                session()->setFlashData('failed', \Config\Services::validation()->getErrors());
                return redirect()->back()->withInput();
            }
        }
        echo view('layout/header', $data);
        echo view('data_input');
        echo view('layout/footer');
    }

    public function datainput()
    {
        $data = [
            'bodyclass' => ''
        ];
        echo view('layout/header', $data);
        echo view('data_input');
        echo view('layout/footer');
    }
    public function savedatainput()
    {
        $data['title'] = 'Add Data';
        if ($this->request->getPost()) {
            $rules = [
                'kodewn' => 'required',
                'kk' => 'required',
                'sex' => 'required',
                'tmplahir' => 'required',
                'tgllahir' => 'required',
                'jalan' => 'required',
                'kodepropinsi' => 'required',
                'kodekota' => 'required',
                'kec' => 'required',
                'kel' => 'required',
                'statusnikah' => 'required',
                'kodeagama' => 'required',
                'ukuranalmamater' => 'required',
                'namaayah' => 'required',
                'namaibu' => 'required',
                'jalanortu' => 'required',
                'kodepropinsiortu' => 'required',
                'kodekotaortu' => 'required',
                'kecortu' => 'required',
                'kelortu' => 'required',
                'telportu' => 'required',
                'xasalsmu' => 'required',
                'propinsismu' => 'required',
                'kodekotasmu' => 'required',
                'kodejenissmu' => 'required',
                'alamatsmu' => 'required',
            ];

            if ($this->validate($rules)) {
                $inserted = [
                    'kodewn' => $this->request->getPost('kodewn'),
                    'nomorkk' => $this->request->getPost('nomorkk'),
                    'sex' => $this->request->getPost('sex'),
                    'tmplahir' => $this->request->getPost('tmplahir'),
                    'tgllahir' => $this->request->getPost('tgllahir'),
                    'jalan' => $this->request->getPost('jalan'),
                    'rt' => $this->request->getPost('rt'),
                    'rw' => $this->request->getPost('rw'),
                    'kodepropinsi' => $this->request->getPost('kodepropinsi'),
                    'kodekota' => $this->request->getPost('kodekota'),
                    'kec' => $this->request->getPost('kec'),
                    'kel' => $this->request->getPost('kel'),
                    'statusnikah' => $this->request->getPost('statusnikah'),
                    'kodeagama' => $this->request->getPost('kodeagama'),
                    'ukuranalmamater' => $this->request->getPost('ukuranalmamater'),
                    'namaayah' => $this->request->getPost('namaayah'),
                    'kodepekerjaanayah' => $this->request->getPost('kodepekerjaanayah'),
                    'kodependidikanayah' => $this->request->getPost('kodependidikanayah'),
                    'kodependapatanayah' => $this->request->getPost('kodependapatanayah'),
                    'namaibu' => $this->request->getPost('namaibu'),
                    'kodepekerjaanibu' => $this->request->getPost('kodepekerjaanibu'),
                    'kodependidikanibu' => $this->request->getPost('kodependidikanibu'),
                    'kodependapatanibu' => $this->request->getPost('kodependapatanibu'),
                    'jalanortu' => $this->request->getPost('jalanortu'),
                    'rtortu' => $this->request->getPost('rtortu'),
                    'rwortu' => $this->request->getPost('rwortu'),
                    'kodepropinsiortu' => $this->request->getPost('kodepropinsiortu'),
                    'kodekotaortu' => $this->request->getPost('kodekotaortu'),
                    'kecortu' => $this->request->getPost('kecortu'),
                    'kelortu' => $this->request->getPost('kelortu'),
                    'telportu' => $this->request->getPost('telportu'),
                    'biayadari' => $this->request->getPost('biayadari'),
                    'kontaknama' => $this->request->getPost('kontaknama'),
                    'jalankontak' => $this->request->getPost('jalankontak'),
                    'rtkontak' => $this->request->getPost('rtkontak'),
                    'rwkontak' => $this->request->getPost('rwkontak'),
                    'kodepropinsikontak' => $this->request->getPost('kodepropinsikontak'),
                    'kodekotakotak' => $this->request->getPost('kodekotakotak'),
                    'kontaktelp' => $this->request->getPost('kontaktelp'),
                    'xasalsmu' => $this->request->getPost('xasalsmu'),
                    'propinsismu' => $this->request->getPost('propinsismu'),
                    'kodekotasmu' => $this->request->getPost('kodekotasmu'),
                    'kodejenissmu' => $this->request->getPost('kodejenissmu'),
                    'alamatsmu' => $this->request->getPost('alamatsmu'),
                    'telpsmu' => $this->request->getPost('telpsmu'),
                    'thnmasuksmaasal' => $this->request->getPost('thnmasuksmaasal'),
                    'raport_10_1' => $this->request->getPost('raport_10_1'),
                    'raport_10_2' => $this->request->getPost('raport_10_2'),
                    'raport_11_1' => $this->request->getPost('raport_11_1'),
                    'raport_11_2' => $this->request->getPost('raport_11_2'),
                    'raport_12_1' => $this->request->getPost('raport_12_1'),
                    'ptasal' => $this->request->getPost('ptasal'),
                    'nimptasal' => $this->request->getPost('nimptasal'),
                    'propinsiptasal' => $this->request->getPost('propinsiptasal'),
                    'ptjurusan' => $this->request->getPost('ptjurusan'),
                    'ptthnlulus' => $this->request->getPost('ptthnlulus'),
                    'ptipk' => $this->request->getPost('ptipk'),
                    'pernahponses' => $this->request->getPost('pernahponses'),
                    'namaponpes' => $this->request->getPost('namaponpes'),
                    'alamatponpes' => $this->request->getPost('alamatponpes'),
                    'propinsiponpes' => $this->request->getPost('propinsiponpes'),
                    'kodekotaponpes' => $this->request->getPost('kodekotaponpes'),
                    'lamaponpes' => $this->request->getPost('lamaponpes'),
                    'namarekom' => $this->request->getPost('namarekom'),
                    'tahuunmerdari' => $this->request->getPost('tahuunmerdari')

                ];

                $this->daftarModel->insert($inserted);
                session()->setFlashData('success', 'data has been added to database');
                return redirect()->to('/datainput');
            } else {
                session()->setFlashData('failed', \Config\Services::validation()->getErrors());
                return redirect()->back()->withInput();
            }
        }
        echo view('layout/header', $data);
        echo view('data_input');
        echo view('layout/footer');
    }

    // public function delete_data($id)
    // {
    //     $photoId = $this->objectModel->find($id);
    //     unlink('photos/' . $photoId['photo']);

    //     $this->objectModel->delete($id);
    //     session()->setFlashData('success', 'data has been deleted from database.');
    //     return redirect()->to('/objects');
    // }

    // public function update_data($id)
    // {
    //     $data = [
    //         'title' => 'Update Data',
    //         'dataById' => $this->objectModel->where('id', $id)->first()
    //     ];

    //     if ($this->request->getPost()) {
    //         $rules = [
    //             'name' => 'required|alpha_space',
    //             'gender' => 'required',
    //             'address' => 'required',
    //             'photo' => 'max_size[photo,2048]|is_image[photo]|mime_in[photo,image/jpg,image/jpeg,image/png]'
    //         ];

    //         if ($this->validate($rules)) {
    //             $photo = $this->request->getFile('photo');
    //             if ($photo->getError() == 4) {
    //                 $photoName = $this->request->getPost('Oldphoto');
    //             } else {
    //                 $photoName = $photo->getRandomName();
    //                 $photo->move('photos', $photoName);
    //                 $photo = $this->objectModel->find($id);
    //                 if ($photo['photo'] == $photo['photo']) {
    //                     unlink('photos/' . $this->request->getPost('Oldphoto'));
    //                 }
    //             }

    //             $inserted = [
    //                 'name' => $this->request->getPost('name'),
    //                 'gender' => $this->request->getPost('gender'),
    //                 'address' => $this->request->getPost('address'),
    //                 'photo' => $photoName
    //             ];

    //             $this->objectModel->update($id, $inserted);
    //             session()->setFlashData('success', 'data has been updated from database');
    //             return redirect()->to('/objects');
    //         } else {
    //             session()->setFlashData('failed', \Config\Services::validation()->getErrors());
    //             return redirect()->back()->withInput();
    //         }
    //     }
    //     return view('objects/form_update', $data);
    // }
}
