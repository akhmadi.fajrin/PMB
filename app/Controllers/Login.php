<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Login extends BaseController
{
    public function index()
    {
        $data = [
            'bodyclass' => 'fullscreen-bg'
        ];
        echo view('layout/header', $data);
        echo view('login');
        echo view('layout/footer');
    }
}
