<?php

namespace App\Controllers;

use CodeIgniter\Controller;
// use App\Models\DaftarModel;

class DataInputController extends Controller
{
    public function index()
    {
        $data = [
            'bodyclass' => ''
        ];

        echo view('layout/header', $data);
        echo view('data_input', $data);
        echo view('layout/footer');
    }

    public function save()
    {

        helper(['form', 'url']);

        $val = $this->validate([
            'kodewn' => 'required',
            'kk' => 'required',
            'sex'  => 'required',
            'tmplahir'  => 'required',
            'jalan' => 'required',
            'kodepropinsi'  => 'required',
            'kodekota' => 'required',
            'statusnikah' => 'required',
            'kodeagama' => 'required',
            'ukuranalmamater' => 'required',
            'namaayah' => 'required',
            'namaibu' => 'required',
            'kodepropinsiortu' => 'required',
            'kodekotaortu' => 'required',
            'kecortu' => 'required',
            'kelortu' => 'required',
            'telportu' => 'required',
            'xasalsmu' => 'required',
            'propinsismu' => 'required',
            'kodekotasmu' => 'required',
            'kodejenissmu' => 'required',
            'alamatsmu' => 'required',
            'raport_10_1' => 'required',
            'raport_10_2' => 'required',
            'raport_11_1' => 'required',
            'raport_11_2' => 'required',
            'raport_12_1' => 'required'
        ]);

        $model = new DaftarModel();
        if (!$val) {
            $data = [
                'bodyclass' => 'fullscreen-bg fbg-v2'
            ];

            echo view('layout/header', $data);
            echo view('daftar', [
                'validation' => $this->validator
            ]);
            echo view('layout/footer');
        } else {
            $model->save([
                'nama' => $this->request->getVar('nama'),
                'nomorktp'  => $this->request->getVar('nomorktp'),
                'nisn'  => $this->request->getVar('nisn'),
                'hp'  => $this->request->getVar('hp')
            ]);
            echo view('welcome_message');
        }
    }
}