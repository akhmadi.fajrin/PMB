<?php

namespace App\Controllers;

use App\Models\Pendaftaran_model;
use CodeIgniter\Controller;

class Pages extends BaseController
{

    protected $Pendaftaran_model;

    public function __construct()
    {
        $this->Pendaftaran_model = new Pendaftaran_model();
    }
    public function index()
    {
        return view('layout/template');
    }
    public function contoh()
    {
        $data = $this->Pendaftaran_model->selectdatadaftar();
        dd($data);


        // echo view('layout/header');
        echo view('contoh');
        echo view('layout/footer');
    }
    public function daftar_1()
    {

        $data = [
            'bodyclass' => 'fullscreen-bg'

        ];
        echo view('layout/header', $data);
        echo view('daftar_awal');
        echo view('layout/footer');
    }
    public function daftar_2()
    {

        $data = [
            'bodyclass' => 'fullscreen-bg h-auto fbg-v2'

        ];
        echo view('layout/header', $data);
        echo view('jalur_penerimaan');
        echo view('layout/footer');
    }
    public function daftar_3()
    {

        $data = [
            'bodyclass' => 'fullscreen-bg fbg-v2'

        ];
        echo view('layout/header', $data);
        echo view('data_prodi');
        echo view('layout/footer');
    }
    public function daftar_4()
    {

        $data = [
            'bodyclass' => 'fullscreen-bg fbg-v2'
        ];

        // $Pendaftaran_model = new Pendaftaran_model();
        // $Pendaftaran = $Pendaftaran_model->findAll();
        // dd($Pendaftaran);

        echo view('layout/header', $data);
        echo view('daftar', $data);
        echo view('layout/footer');
    }
    public function form_action()
    {
        $data['mhs'] = $this->Pendaftaran_model->insertdatadaftar();
    }
    public function token()
    {
        $data = [
            'bodyclass' => 'fullscreen-bg'
        ];
        echo view('layout/header', $data);
        echo view('daftar_token');
        echo view('layout/footer');
    }
    public function pengumuman()
    {
        $data = [
            'bodyclass' => ''
        ];
        echo view('layout/header', $data);
        echo view('pengumuman_lulus');
        echo view('layout/footer');
    }
    public function tagihan()
    {
        $data = [
            'bodyclass' => ''
        ];
        echo view('layout/header', $data);
        echo view('tagihan_pendaftar');
        echo view('layout/footer');
    }
    public function data_input()
    {
        $data = [
            'bodyclass' => ''
        ];
        echo view('layout/header', $data);
        echo view('data_input');
        echo view('layout/footer');
    }
    public function dashboard()
    {
        $data = [
            'bodyclass' => ''
        ];
        echo view('layout/header', $data);
        echo view('dash_pendaftar');
        echo view('layout/footer');
    }
    public function payment()
    {
        $data = [
            'bodyclass' => ''
        ];
        echo view('layout/header', $data);
        echo view('list_payment');
        echo view('layout/footer');
    }

    public function data_peminat()
    {
        $data = [
            'bodyclass' => ''
        ];
        echo view('layout/header', $data);
        echo view('data_peminat');
        echo view('layout/footer');
    }
}