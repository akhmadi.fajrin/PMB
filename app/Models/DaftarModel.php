<?php

namespace App\Models;

use CodeIgniter\Model;

class DaftarModel extends Model
{
    protected $table         = 'pendaftaran.pd_pendaftar';
    // protected $table         = 'pendaftaran';
    protected $primaryKey    = 'id';
    protected $returnType    = 'array';
    protected $allowedFields = ['jalurpenerimaan', 'periodedaftar', 'idgelombang', 'email', 'sistemkuliah', 'pilihan1', 'pilihan2', 'nama', 'nomorktp', 'nis', 'hp', 'kodewn', 'nomorkk', 'sex', 'tmplahir', 'tgllahir', 'jalan', 'rt', 'rw', 'kodepropinsi', 'kodekota', 'kec', 'kel', 'statusnikah', 'kodeagama', 'ukuranalmamater', 'namaayah', 'kodepekerjaanayah', 'kodependidikanayah', 'kodependapatanayah', 'namaibu', 'kodepekerjaanibu', 'kodependidikanibu', 'kodependapatanibu', 'jalanortu', 'rtortu', 'rwortu', 'kodepropinsiortu', 'kodekotaortu', 'kecortu', 'kelortu', 'telportu', 'biayadari', 'kontaknama', 'jalankontak', 'rtkontak', 'rwkontak', 'kodepropinsikontak', 'kodekotakotak', 'kontaktelp', 'xasalsmu', 'propinsismu', 'kodekotasmu', 'kodejenissmu', 'alamatsmu', 'telpsmu', 'thnmasuksmaasal', 'raport_10_1', 'raport_10_2', 'raport_11_1', 'raport_11_2', 'raport_12_1', 'ptasal', 'nimptasal', 'propinsiptasal', 'ptjurusan', 'ptthnlulus', 'ptipk', 'pernahponses', 'namaponpes', 'alamatponpes', 'propinsiponpes', 'kodekotaponpes', 'lamaponpes', 'namarekom', 'tahuunmerdari'];
    // protected $useTimestamps = true;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
}
