<main class="form-center px-5 py-5">
    <!-- Content here -->
    <div class="text-center">
        <img class="img-fluid mb-3" src="<?= base_url(); ?>/assets/img/logo.png" alt="Image Description">
        <h1 class="fs-3">Buat Akun Pendaftaran<br> Mahasiswa Baru <span class="txt-orange">Unmer Malang</span></h1>
        <p>Silahkan menggunakan akun google untuk mendaftar.</p>
    </div>
    <div class="google-btn">
        <div id="my-signin2"></div>
        <script>
        function onSuccess(googleUser) {
            console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
        }

        function onFailure(error) {
            console.log(error);
        }

        function renderButton() {
            gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': 275,
                'height': 50,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure,
                'loginWithLabel': 'Daftar dengan %s'
            });
        }
        </script>

        <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    </div>
</main>