<header>
    <div class="container">
        <div class="logo"><img class="img-fluid" src="<?= base_url(); ?>/assets/img/unmer.png" alt="Image Description">
        </div>
        <div class="phone"></div>
        <div class="logout">Logout</div>
    </div>
</header>
<div class="container">
    <div class="status-pendaftar">
        <button type="submit" class="btn btn-primary" disabled>1. Pilih Jalur</button>
        <button type="submit" class="btn btn-warning" disabled>2. Pembayaran</button>
        <button type="submit" class="btn btn-secondary" disabled>3. Isi Formulir</button>
        <button type="submit" class="btn btn-secondary" disabled>4. Berhasil Daftar</button>
    </div>
    <br>
    <h2 class="page-title">
        Pembayaran Formulir dengan Bank (..)
    </h2>
    <div class="col-md-9">
        <div class="card">
            <h5 class="card-header">
                Cara Pembayaran Formulir dengan $data_payment['data_pay']
            </h5>
            <div class="card-body">
                <ol>
                    <li>Proses pembayaran bisa dilakukan dengan transfer ke rekening $data_payment['data_pay'], melalui ATM, E-banking, Mobile-banking, dll (transfer juga bisa menggunakan selain $data_payment['data_pay']</li>
                    <li>Total jumlah tagihan yang dibayarkan = [jumlah tarif formulir + biaya administrasi] misal Rp.250.000 + Rp.2.500, biaya administrasi sewaktu - waktu bisa berubah</li>
                    <li>Kode VA = No rekening tujuan transfer</li>
                    <li>Simpan baik - baik KODE VA yang keluar setelah anda melakukan pemesanan formulir, karena akan anda gunakan untuk proses pendaftaran selanjutnya (Token akan di isi dengan Kode VA)</li>
                    <li>Setelah pembayaran dilakukan pendaftar bisa melakukan proses pengisian formulir pendaftaran</li>
                </ol>
            </div>
        </div>
    </div>
    <br>
    <div class="col-md-3">
        <button type="button" class="btn btn-primary" i class="far fa-user pr-2">Simpan</button>
        <button type="button" class="btn btn-danger" i class="far fa-user pr-2">Batal</button>
    </div>
    <br>
    <div class="col-md-9">
        <div class="card">
            <h5 class="card-header">
                Detail Pendaftaran
            </h5>
            <div class="card-body">
                <small class="text-smooth inline">Jalur Penerimaan :</small>
                <p>$pendaftar['jalur'] ?></p>
                <small class="text-smooth inline">Periode :</small>
                <p>$pendaftar['periode'] ?></p>
                <small class="text-smooth inline">Gelombang :</small>
                <p> $pendaftar['gelombang'] ?></p>
                <small class="text-smooth inline">Jenjang :</small>
                <p> $pendaftar['jenjang'] ?></p>
            </div>
        </div>
        <br>
        <div class="card">
            <h5 class="card-header">
                Data Pemesan Formulir
            </h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-25">
                        <label for="nama">Nama *</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="nama" name="nama">
                    </div>
                    <div class="col-25">
                        <label for="hp">No HP *</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="hp" name="hp">
                    </div>
                    <div class="col-25">
                        <label for="email">E-mail *</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="email" name="email">
                    </div>
                    <div class="col-25">
                        <label for="pil-form">Pilihan Formulir *</label>
                    </div>
                    <div class="col-75">
                        <select id="pilform" name="pilform">
                            <option value="1">?>['pilform']</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>