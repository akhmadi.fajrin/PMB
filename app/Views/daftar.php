<main class="form-center px-5 py-5 white-bg">
    <!-- Content here -->
    <div class="text-center">
        <img class="img-fluid mb-3" src="<?= base_url(); ?>/assets/img/logo.png" alt="Image Description">
        <h1 class="fs-3">Pendaftaran Penerimaan<br> Mahasiswa Baru <span class="txt-orange">Unmer Malang</span></h1>
        <p>Buat Akun Pendaftaran Unmer Malang.</p>
    </div>
    <?php echo \Config\Services::validation()->listErrors() ?>

    <form action="<?= base_url('DaftarController/savedatainputsatu'); ?>" class="form-daftar" method="post" novalidate>
        <div class="form-floating">
            <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" required onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || event.charCode == 32 || event.charCode == 45 || event.charCode == 46 || event.charCode == 96)">
            <label for="nama">Nama Lengkap</label>
        </div>
        <div class="form-floating">
            <input type="text" class="form-control" placeholder="Nomor Induk Kependudukan(NIK)" name="nomorktp" required onkeypress="return numberOnly(event)" maxlength="16">
            <label for="nomorktp">Nomor Induk Kependudukan(NIK)</label>
        </div>
        <div class="form-floating">
            <input type="text" class="form-control" placeholder="Nomor Induk Siswa Nasional(NISN)" name="nisn" required onkeypress="return numberOnly(event)" maxlength="10">
            <label for="nisn">Nomor Induk Siswa Nasional(NISN)</label>
        </div>
        <div class="form-floating">
            <input type="text" class="form-control" placeholder="Nomor Handphone" name="hp" required onkeypress="return numberOnly(event)">
            <label for="hp">Nomor Handphone</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary btn-green fs-5" type="submit">Simpan</button>
    </form>

</main>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script>
    function numberOnly(evt) {
        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }
    $(function() {
        jQuery.extend(jQuery.validator.messages, {
            required: "Field harus diisi"
        });
        $('.form-daftar').validate({
            rules: {
                nomotktp: {
                    required: true,
                    maxlength: 16,
                    minlength: 16,

                }
            },
            messages: {
                nomorktp: {
                    minlength: "NIK tidak boleh kurang dari 16 karakter",
                    maxlength: "NIK tidak boleh lebih dari 16 karakter"
                }
            },
            errorElement: "div",
            errorPlacement: function(error, element) {
                error.addClass("invalid-feedback");
                error.insertAfter(element);
            },
            highlight: function(element) {
                $(element).removeClass('is-valid').addClass('is-invalid');
            },
            unhighlight: function(element) {
                $(element).removeClass('is-invalid').addClass('is-valid');
            }
        });
    });
</script>