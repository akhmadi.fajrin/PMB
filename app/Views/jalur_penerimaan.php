<div class="ec-bg"></div>
<main style="max-width: 1000px; margin: auto;">
    <div class="container alur-penerimaan">
        <div class="row">
            <div class="text-center">
                <img class="img-fluid mb-3" src="<?= base_url(); ?>/assets/img/logo.png" alt="Image Description">
                <h1 class="fs-2">Jalur Penerimaan</h1>
                <h5>Jalur Penerimaan Yang Sedang Dibuka</h5>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Undangan</h4>
                        Untuk siswa SMA/SMK/MA yang lulus di tahun 2022 dengan nilai rata-rata raport semester 1 sampai
                        5 minimal 82.5
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange" data-bs-toggle="modal"
                            data-bs-target="#exampleModalFullscreen">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Sukses Vokasi</h4>
                        Program masuk D3 untuk siswa SMK yang lulus di tahun 2022 dengan nilai rata-rata raport semester
                        1 sampai 5 minimal 80, dan berasal dari keluarga pra sejahtera (dibuktikan dengan SKTM)
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Prestasi Akademik</h4>
                        Untuk siswa SMA/SMK/MA yang lulus di tahun 2019-2022 dengan nilai rata-rata raport semester 1
                        sampai 5 minimal 85
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Prestasi Non Akademik</h4>
                        Untuk siswa SMA/SMK/MA yang lulus di tahun 2019-2022 dengan nilai rata-rata raport semester 1
                        sampai 5 minimal 75 dan memiliki prestasi dalam bidang olahraga atau seni yang dibuktikan dengan
                        piagram prestasi minimal Juara 1 Tingkat Kabupaten/Kota
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Non Tes</h4>
                        Untuk siswa SMA/SMK/MA yang lulus di tahun 2019-2022 dengan nilai rata-rata raport semester 1
                        sampai 5 minimal 80
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Tes</h4>
                        Untuk siswa SMA/SMK/MA/ Ujian Penyetaraan/ Homeschooling yang telah lulus dengan nilai rata-rata
                        raport semester 1 sampai 5 Kurang dari 80, atau lulusan tahun 2018 dan sebelumnya
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Alih Jenjang Dalam</h4>
                        Untuk alumni D3 di Universitas Merdeka Malang yang akan melanjutkan ke D4 atau S1
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Alih Jenjang Luar</h4>
                        Untuk alumni D3 di luar Universitas Merdeka Malang yang akan melanjutkan ke D4 atau S1
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Pindahan Dalam</h4>
                        Untuk mahasiswa aktif yang akan pindah antar prodi yang setara (D3 ke D3 atau D4 dan S1 ke S1)
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h4>Pindahan Luar</h4>
                        untuk mahasiswa aktif dari kampus lain yang akan pindah ke Universitas Merdeka Malang yang
                        setara (D3 ke D3, D4 ke D4, atau S1 ke S1)
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-orange">Daftar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>