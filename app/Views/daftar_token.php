<main class="form-center px-5 py-5">
    <!-- Content here -->
    <div class="text-center">
        <img class="img-fluid mb-3" src="<?= base_url(); ?>/assets/img/logo.png" alt="Image Description">
        <h1 class="fs-3">Pendaftaran Penerimaan<br> Mahasiswa Baru <span class="txt-orange">Unmer Malang</span></h1>
        <p class="text-start">Sebelum melanjutkan, pastikan Anda telah membayar formulir (lihat cara membayar formulir
            <a href="#">disini</a>). Token atau
            kode VA yang digunakan adalah pembelian pada periode 20221 , Gelombang 01 dan Jenjang Sarjana.<br>
            KODE VA yang dapat digunakan untuk pendaftaran adalah kode VA yang telah di bayar di bank BNI.
        </p>
    </div>
    <form>
        <div class="form-floating">
            <input type="text" class="form-control" placeholder="No. Token / Kode VA*" name="nama">
            <label for="nama">No. Token / Kode VA*</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary btn-orange fs-5" type="submit">Daftar Sekarang</button>
    </form>
</main>