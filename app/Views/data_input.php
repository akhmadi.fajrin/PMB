<header>
    <div class="container">
        <button data-trigger="#my_offcanvas1" class="btn btn-primary menu-btn d-lg-none" type="button"></button>
        <div class="logo"><img class="img-fluid" src="<?= base_url(); ?>/assets/img/unmer.png" alt="Image Description">
        </div>
        <div class="phone"></div>
        <div class="logout">Logout</div>
    </div>
</header>
<b class="screen-overlay"></b>
<div class="container">
    <div class="row g-5">
        <div class="col-md-3">

            <div class="sidebar-menu d-none d-lg-block">
                <ul>
                    <li><a href="#" class="profil-anda disabled">Profil Anda</a></li>
                    <li><a href="#" class="tagihan">Tagihan Pendaftar</a></li>
                    <li><a href="#" class="cetak-kartu">Cetak Kartu Ujian</a></li>
                    <li><a href="#" class="cetak-formulir">Cetak Formulir Pendaftaran</a></li>
                    <li><a href="#" class="ganti-pass">Ganti Password</a></li>
                    <li><a href="#" class="pengumuman">Pengumuman Kelulusan</a></li>
                    <li><a href="#" class="dokumen current-page">Lengkapi Dokumen</a></li>
                </ul>
            </div>
            <!-- offcanvas panel -->
            <div class="offcanvas" id="my_offcanvas1">
                <header class="p-4 bg-light border-bottom mb-0">
                    <button class="btn btn-outline-danger btn-close"></button>
                </header>
                <div class="sidebar-menu">
                    <ul>
                        <li><a href="#" class="profil-anda disabled">Profil Anda</a></li>
                        <li><a href="#" class="tagihan">Tagihan Pendaftar</a></li>
                        <li><a href="#" class="cetak-kartu">Cetak Kartu Ujian</a></li>
                        <li><a href="#" class="cetak-formulir">Cetak Formulir Pendaftaran</a></li>
                        <li><a href="#" class="ganti-pass">Ganti Password</a></li>
                        <li><a href="#" class="pengumuman">Pengumuman Kelulusan</a></li>
                        <li><a href="#" class="dokumen current-page">Lengkapi Dokumen</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9 mt-mobile-top">
            <?= $this->include('layout/status_pendaftar') ?>
            <h4 class="page-title">Lengkapi Dokumen Pendaftaran</h4>
            <div class="info-block">
                <div class="alert alert-danger" role="alert">Belum Ada Info Terkait Kelulusan!</div>
                <div class="alert alert-success" role="alert">Selamat Datang! Silahkan melengkapi formulir pendaftaran
                    dibawah ini. <br>
                    Untuk Jalur Prestasi Akademik, nilai Rata-rata minimal Raport harus >= 60. <br>
                    Pengisian formulir ini memiliki batasan waktu satu jam, pergunakan dengan baik.</div>
            </div>

            <br>
            <div class="card">
                <h5 class="card-header">
                    Detail Pendaftaran
                </h5>
                <div class="card-body">
                    <div class="text-smooth text-block">Jalur Penerimaan :</div>
                    <p> $pendaftar['jalur'] ?></p>
                    <div class="text-smooth text-block">Periode :</div>
                    <p> $pendaftar['periode'] ?></p>
                    <div class="text-smooth text-block">Gelombang :</div>
                    <p> $pendaftar['gelombang'] ?></p>
                </div>
            </div>

            <!-- <br> -->
            <!-- <div class="card">
                <h5 class="card-header">
                    Data Pendaftar
                </h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-25">
                            <label for="nama">Nama Pendaftar</label>
                        </div>
                        <div class="col-75">
                            <input type="text" id="nama" name="nama" placeholder="?>namapendaftar" disabled>
                        </div>
                        <div class="col-25">
                            <label for="nama">Sistem Kuliah</label>
                        </div>
                        <div class="col-75">
                            <select id="sistemkuliah" name="sistemkuliah">
                                <option value="1">Reguler Pagi</option>
                                <option value="2">Reguler Sore</option>
                                <option selected>-- Pilih Sistem Kuliah --</option>
                            </select>
                        </div>
                        <div class="col-25">
                            <label for="nama">Pilihan Prodi</label>
                        </div>
                        <div class="col-75">
                            <select id="pilprodi" name="pilprodi">
                                <option value="1">S1 Hukum</option>
                                <option value="1">S1 Ilmu Sosial</option>
                                <option selected>-- Pilih Program Studi --</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div> -->

            <br>

            <div class="info-block">
                <div class="alert alert-success" role="alert">Harap mengisi tab <strong>Data Pendaftar</strong>,
                    <strong>Data Keluarga</strong> dan
                    Tab <strong>Data Sekolah</strong>.
                </div>
            </div>
            <!-- tab navigation -->
            <div class="col-md-13 col-sm-16">
                <!-- <div class="form-step">Pendaftar > Keluarga > Sekolah > Rekomendasi</div> -->
                <ul class="nav nav-pills mb-3 mt-6" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <button class="nav-link active" id="pills-biodata-tab" data-bs-toggle="pill"
                            data-bs-target="#biodata" type="button" role="tab" aria-controls="pills-home"
                            aria-selected="true">Data Pendaftar</button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link" id="pills-biodata-tab" data-bs-toggle="pill"
                            data-bs-target="#informasi" type="button" role="tab" aria-controls="informasi"
                            aria-selected="false">Data Keluarga</button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link" id="pills-biodata-tab" data-bs-toggle="pill" data-bs-target="#akademik"
                            type="button" role="tab" aria-controls="akademik" aria-selected="false">Data
                            Sekolah</button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link" id="pills-biodata-tab" data-bs-toggle="pill"
                            data-bs-target="#rekomendasi" type="button" role="tab" aria-controls="rekomendasi"
                            aria-selected="false">Rekomendasi</button>
                    </li>
                </ul>
                <form action="#" class="form-daftar" novalidate>
                    <div class="tab-content" id="pills-tabContent">
                        <!-- tab content data pendaftar -->
                        <div class="tab-pane fade show active" id="biodata" role="tabpanel" aria-labelledby="biodata">
                            <?= $this->include('layout/front/data_pendaftar') ?>
                        </div>

                        <!-- tab data keluarga -->
                        <div class="tab-pane fade" id="informasi" role="tabpanel" aria-labelledby="informasi">
                            <?= $this->include('layout/front/data_keluarga') ?>
                        </div>

                        <!-- tab data sekolah -->
                        <div class="tab-pane fade" id="akademik" role="tabpanel" aria-labelledby="akademik">
                            <?= $this->include('layout/front/data_sekolah') ?>
                        </div>

                        <!-- tab data sekolah -->
                        <div class="tab-pane fade" id="pendidikan" role="tabpanel" aria-labelledby="pendidikan">
                            <?= $this->include('layout/front/data_sekolah') ?>
                        </div>

                        <!-- tab content data rekomendasi -->
                        <div class="tab-pane fade" id="rekomendasi" role="tabpanel" aria-labelledby="rekomendasi">
                            <?= $this->include('layout/front/rekomendasi') ?>
                        </div>
                    </div>

                    <br>
                    <button type="submit" name="submit" class="btn btn-lg btn-primary btn-green"
                        style="margin-right: 10px;">Simpan</button>
                    <button type="button" name="button" class="btn btn-lg btn-danger btn-red">Batal</button>

                </form>

            </div>

            <!-- <div class="card">
                <h5 class="card-header">Konfirmasi</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-25">
                            <label for="kodekonfirm">Kode Konfirmasi</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="kodekonfirm">
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<footer>
    <div class="footer-bottom">
        <div class="container">
            <div class="row ">
                <span>Copyright &copy; 2014 - <?php echo date("Y"); ?> Universitas Merdeka Malang. <em>All
                        rights
                        reserved.</em></span>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/@emretulek/jbvalidator"></script> -->

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>

<script>
// mobile menu
$("[data-trigger]").on("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
    var offcanvas_id = $(this).attr('data-trigger');
    $(offcanvas_id).toggleClass("show");
    $('body').toggleClass("offcanvas-active");
    $(".screen-overlay").toggleClass("show");
});

$(".btn-close, .screen-overlay").click(function(e) {
    $(".screen-overlay").removeClass("show");
    $(".offcanvas").removeClass("show");
    $("body").removeClass("offcanvas-active");
});

//cek pesantren
function cekPonPes() {
    var radval = $('input[name="pernahponpes"]:checked').val();
    if (radval == 0) {
        // console.log(radval);
        $('#block-namaponpes').hide();
        $('#block-alamatponpes').hide();
        $('#block-propinsiponpes').hide();
        $('#block-lamaponpes').hide();
        $('#block-kodekotaponpes').hide();

    } else {
        $('#block-namaponpes').show();
        $('#block-alamatponpes').show();
        $('#block-propinsiponpes').show();
        $('#block-lamaponpes').show();
        $('#block-kodekotaponpes').show();

    }
}
</script>

<script>
$(function() {
    // Jquery validation
    jQuery.extend(jQuery.validator.messages, {
        required: "Field harus diisi"
    });
    $('.form-daftar').validate({
        ignore: [],
        errorElement: "div",
        errorPlacement: function(error, element) {
            error.addClass("invalid-feedback");
            error.insertAfter(element);
        },
        highlight: function(element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function(element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                if (validator.errorList.length > 0) {
                    for (x = 0; x < validator.errorList.length; x++) {
                        // errors += validator.errorList[x].message;
                    }
                }
                alert("Isi form belum lengkap, silahkan cek kembali isian form anda.");
            }
        }
    });
})
</script>