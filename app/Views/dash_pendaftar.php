<header>
    <div class="container">
        <div class="logo"><img class="img-fluid" src="<?= base_url(); ?>/assets/img/unmer.png" alt="Image Description">
        </div>
        <div class="phone"></div>
        <div class="logout">Logout</div>
    </div>
</header>
<div class="container">
    <?= $this->include('layout/status_pendaftar') ?>

    <h1 class="page-title">Profil Anda</h1>
    <div class="row g-5">

        <div class="col-md-3 col-sm-12">
            <div class="sidebar-menu">
                <ul>
                    <li><a href="#" class="profil-anda current-page">Profil Anda</a></li>
                    <li><a href="#" class="tagihan">Tagihan Pendaftar</a></li>
                    <li><a href="#" class="cetak-kartu">Cetak Kartu Ujian</a></li>
                    <li><a href="#" class="cetak-formulir">Cetak Formulir Pendaftaran</a></li>
                    <li><a href="#" class="ganti-pass">Ganti Password</a></li>
                    <li><a href="#" class="pengumuman">Pengumuman Kelulusan</a></li>
                    <li><a href="data_input" class="dokumen">Lengkapi Biodata</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-sm-12">
            <div class="card no-border">
                <h5 class="card-header">
                    Data Pendaftar
                </h5>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-bordered m-0">
                            <tbody>
                                <tr>
                                    <td><b>No.Pendaftar </b></td>
                                    <td colspan="3" style="border-top:0px;"><span id="show">202210170</span><span
                                            id="edit" style="display:none">202210170
                                        </span></td>
                                    <td align="center" valign="middle" rowspan="10">
                                        <img id="imgfoto" border="1"
                                            src="../back/uploads/fotocamaba/20221-Non Tes-01/202210170.jpg?r=94704"
                                            style="cursor:pointer;width:2.6cm;height:3.6cm">

                                        <!-- upload photo -->
                                        <div id="popFoto" class="menubar"
                                            style="position:absolute; display:none; z-index: 100">
                                            <table width="120" class="menu-body">
                                                <tbody>
                                                    <tr class="menu-button" onmousemove="this.className='hover'"
                                                        onmouseout="this.className=''">
                                                        <td onclick="setUpload()">Upload Foto</td>
                                                    </tr>
                                                    <tr class="menu-button" onmousemove="this.className='hover'"
                                                        onmouseout="this.className=''">
                                                        <td onclick="goHapusFoto()">Hapus Foto</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <span style="display:none">
                                            <input type="file" name="foto" id="foto" size="10" class="form-control"
                                                onchange="chooseFile()">
                                        </span>

                                        <iframe name="upload_iframe" style="display:none"> </iframe>

                                        <div id="imgfoto_dark" class="Darken" style="display:none"></div>

                                        <div id="imgfoto_light" align="center" class="Lighten" style="display:none">
                                            <img src="images/loading.gif">
                                        </div>
                                        <br>
                                        <small>untuk upload / hapus foto klik foto</small>
                                        <br>
                                        <div class="btn-cam">
                                            <button type="button" class="btn btn-primary" onclick="setUpload()"><span
                                                    class="fa fa-upload"></span> Upload Foto</button>
                                            <span class="material-icons cam">photo_camera</span>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Nama Lengkap</b></td>
                                    <td>
                                        <small class="text-block">Nama Pendaftar :</small>
                                        <span id="show">M RIZKI MAULANA</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2" style="vertical-align: middle;"><b>Pilihan Prodi</b></td>
                                    <td>
                                        <small class="text-block">Pilihan 1 * :</small>
                                        <span>Prodi S1 Hukum</span>
                                        <input type="hidden" name="pilihan1" value="110131">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-block">Pilihan 2 * : </small>
                                        <span></span>
                                        <input type="hidden" name="pilihan2" value="">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle"><b>Program Pendidikan</b></td>
                                    <td colspan="3" style="vertical-align:middle">Sarjana</td>
                                </tr>

                                <tr>
                                    <td style="vertical-align:middle"><b>Sistem Kuliah</b></td>
                                    <td colspan="3" style="vertical-align:middle">R</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle"><b>Jalur Penerimaan </b></td>
                                    <td colspan="3" style="vertical-align:middle">Non Tes</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle"><b>Periode Daftar </b></td>
                                    <td colspan="3" style="vertical-align:middle">20221</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle"><b>Gelombang</b></td>
                                    <td colspan="3" style="vertical-align:middle">01</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <!-- tab navigation -->
            <ul class="nav nav-pills mb-3 mt-5" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="pills-biodata-tab" data-bs-toggle="pill"
                        data-bs-target="#biodata" type="button" role="tab" aria-controls="pills-home"
                        aria-selected="true">Data Pendaftar</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="pills-informasi-tab" data-bs-toggle="pill" data-bs-target="#informasi"
                        type="button" role="tab" aria-controls="informasi" aria-selected="false">Data Keluarga</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="pills-akademik-tab" data-bs-toggle="pill" data-bs-target="#akademik"
                        type="button" role="tab" aria-controls="akademik" aria-selected="false">Data Sekolah</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="pills-pendidikan-tab" data-bs-toggle="pill"
                        data-bs-target="#pendidikan" type="button" role="tab" aria-controls="pendidikan"
                        aria-selected="false">Data Pendidikan/Prestasi</button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="pills-rekomendasi-tab" data-bs-toggle="pill"
                        data-bs-target="#rekomendasi" type="button" role="tab" aria-controls="rekomendasi"
                        aria-selected="false">Rekomendasi</button>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
                <!-- tab content data pendaftar -->
                <div class="tab-pane fade show active" id="biodata" role="tabpanel" aria-labelledby="biodata">
                    <?= $this->include('layout/data_pendaftar') ?>
                </div>

                <!-- tab data keluarga -->
                <div class="tab-pane fade" id="informasi" role="tabpanel" aria-labelledby="informasi">
                    <?= $this->include('layout/data_keluarga') ?>
                </div>

                <!-- tab data sekolah -->
                <div class="tab-pane fade" id="akademik" role="tabpanel" aria-labelledby="akademik">
                    <?= $this->include('layout/data_sekolah') ?>
                </div>

                <!-- tab data sekolah -->
                <div class="tab-pane fade" id="pendidikan" role="tabpanel" aria-labelledby="pendidikan">
                    <?= $this->include('layout/data_pendidikan') ?>
                </div>

                <!-- tab content data rekomendasi -->
                <div class="tab-pane fade" id="rekomendasi" role="tabpanel" aria-labelledby="rekomendasi">
                    <?= $this->include('layout/rekomendasi') ?>
                </div>
            </div>

        </div>
    </div>
</div>

<footer>
    <div class="footer-bottom">
        <div class="container">
            <div class="row ">
                <span>Copyright &copy; 2014 - <?php echo date("Y"); ?> Universitas Merdeka Malang. <em>All
                        rights
                        reserved.</em></span>
            </div>
        </div>
    </div>
</footer>