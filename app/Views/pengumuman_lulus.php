<header>
    <div class="container">
        <div class="logo"><img class="img-fluid" src="<?= base_url(); ?>/assets/img/unmer.png" alt="Image Description">
        </div>
        <div class="phone"></div>
        <div class="logout">Logout</div>
    </div>
</header>
<div class="container">
    <?= $this->include('layout/status_pendaftar') ?>

    <h1 class="page-title">Pengumuman Kelulusan</h1>
    <div class="row g-5">

        <?= $this->include('layout/sidebar') ?>

        <div class="col-md-9">
            <div class="info-block">
                <div class="alert alert-danger" role="alert">Belum Ada Info Terkait Kelulusan!</div>
                <div class="alert alert-success" role="alert">Anda Lolos pada tahap Administrasi!</div>
            </div>
            <div class="card">
                <h5 class="card-header">
                    Informasi Lebih Lanjut
                </h5>
                <div class="card-body">
                    <small class="text-smooth text-block">No. Pendaftar :</small>
                    <p> $pendaftar['nopendaftar'] ?></p>
                    <small class="text-smooth text-block">Nama :</small>
                    <p> $pendaftar['nama'] ?></p>
                    <small class="text-smooth text-block">Alamat :</small>
                    <p>Jln. $pendaftar['jalan'] ?> RT. $pendaftar['rt'] ?> RW. $pendaftar['rw'] ?> Kelurahan
                        $pendaftar['kel'] ?>
                        Kecamatan $pendaftar['kec'] ?>, $mkota[$pendaftar['kodekota']] ?>,
                        $mprop[$pendaftar['kodepropinsi']] ?></p>
                    <small class="text-smooth text-block">Asal SMA :</small>
                    <p> $list_smu[$pendaftar['asalsmu']] . ", " . $list_alamatsmu[$pendaftar['asalsmu']] ?></p>
                </div>
                <div class="card-footer">
                    <a href="#" target="_blank" class="btn btn-green">Download Informasi Daftar Ulang</a>
                    <a href="#" target="_blank" class="btn btn-orange">Cetak Biodata</a>
                    <a href="#" target="_blank" class="btn btn-orange">Cetak KTM Sementara</a>

                </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="footer-bottom">
        <div class="container">
            <div class="row ">
                <span>Copyright &copy; 2014 - <?php echo date("Y"); ?> Universitas Merdeka Malang. <em>All rights
                        reserved.</em></span>
            </div>
        </div>
    </div>
</footer>