<header>
    <div class="container">
        <div class="logo"><img class="img-fluid" src="<?= base_url(); ?>/assets/img/unmer.png" alt="Image Description">
        </div>
        <div class="phone"></div>
        <div class="logout">Logout</div>
    </div>
</header>
<div class="container">
    <?= $this->include('layout/status_pendaftar') ?>

    <h1 class="page-title">Rincian Tagihan Unmer Malang</h1>
    <div class="row g-5">

        <?= $this->include('layout/sidebar') ?>

        <div class="col-md-9">
            <div class="row mb-4">
                <div class="col-md-5">Jumlah cicilan tagihan DPP yang di inginkan</div>
                <div class="col-md-2">
                    <select name="jenisangsurandpp" id="jenisangsurandpp" class="form-select">
                        <option value="1">1x</option>
                        <option value="5" selected="">5x</option>
                    </select>
                </div>
                <div class="col-md-5">
                    <input type="button" value="Pilih jumlah cicilan dan ubah tagihan" class="btn btn-orange"
                        onclick="goUpdateTagihan()">
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th>Jenis Tagihan</th>
                            <th>Angsuran Ke</th>
                            <th>Tanggal Tagihan</th>
                            <th>Tanggal Deadline</th>
                            <th>Kode VA</th>
                            <th>Lunas</th>
                            <th>Nominal</th>
                            <th>Potongan</th>
                        </tr>
                        <tr>
                            <td>HER</td>
                            <td>1</td>
                            <td>01-April-2022</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right">250.000</td>
                            <td align="right">0</td>
                        </tr>

                        <tr>
                            <td>BPP</td>
                            <td>1</td>
                            <td>01-April-2022</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right">3.400.000</td>
                            <td align="right">0</td>
                        </tr>

                        <tr>
                            <td colspan="6"> Total </td>
                            <td align="right">3.650.000</td>
                            <td align="right">0</td>
                        </tr>
                        <tr>
                            <td colspan="6"> Total Tagihan - Potongan </td>
                            <td colspan="2" align="right">3.650.000</td>
                        </tr>
                        <tr>
                            <td colspan="8" class="bg-warning">
                                <i>Biaya transaksi akan keluar setelah anda klik tombol Bayar tagihan di bawah.
                                    <br>Biaya ini akan ditambahkan ke total yang harus dibayar.</i>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4" align="right" style="vertical-align: middle;"> Pilih pembayaran melalui
                            </td>
                            <td colspan="2"> <select name="metodebayar" id="metodebayar"
                                    class="ControlStyle form-select" onchange="goSubmit()">
                                    <option value="bni">BNI</option>
                                    <option value="btn">BTN</option>
                                    <option value="jatim">Bank Jatim</option>
                                </select> </td>
                            <td colspan="2" align="center"><input type="button" class="btn btn-orange"
                                    value="Bayar tagihan" onclick="goBayar()"></td>
                        </tr>
                        <input type="hidden" name="act" id="act">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>