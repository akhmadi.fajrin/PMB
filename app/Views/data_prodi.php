<main class="form-center px-5 py-5 white-bg">
    <div class="text-center">
        <img class="img-fluid mb-3" src="<?= base_url(); ?>/assets/img/logo.png" alt="Image Description">
        <h1 class="fs-3">Pendaftaran Penerimaan<br> Mahasiswa Baru <span class="txt-orange">Unmer Malang</span></h1>
        <p>Buat Akun Pendaftaran Unmer Malang.</p>
    </div>
    <div class="row">
        <form method="post" class="form-daftar" action="<?= base_url('DaftarController/savepilihprodi'); ?>" novalidate>
            <div class="form-floating">
                <input type="text" class="form-control" placeholder="No. Token / Kode VA*" name="tokenpendaftaran" required>
                <label for="tokenpendaftaran">No. Token / Kode VA*</label>
            </div>
            <div class="">
                <label for="nama">Sistem Kuliah</label>
                <select id="sistemkuliah" name="sistemkuliah" required>
                    <option selected value="">-- Pilih Sistem Kuliah --</option>
                    <option value="1">Reguler Pagi</option>
                    <option value="2">Reguler Sore</option>
                </select>
                <div>
                    <em><small>Sistem kuliah yang anda pilih akan mempangaruhi pilihan prodi. Prodi yang tampil adalah prodi yang sedang di buka di sistem kuliah yg dipilih dan pagu masih mencukupi.</small></em>
                </div>
            </div>
            <div class="">
                <label for="nama">Pilihan Prodi 1</label>
                <select id="pilihan1" name="pilihan1" required>
                    <option selected value="">-- Pilih Program Studi --</option>
                    <option value="1">S1 Hukum</option>
                    <option value="2">S1 Ilmu Sosial</option>
                </select>
            </div>
            <div class="">
                <label for="nama">Pilihan Prodi 2</label>
                <select id="pilihan2" name="pilihan2" required>
                    <option selected value="">-- Pilih Program Studi --</option>
                    <option value="1">S1 Hukum</option>
                    <option value="2">S1 Ilmu Sosial</option>
                </select>
            </div>
            <button class="w-100 btn btn-lg btn-primary btn-green mt-3" type="submit">Selanjutnya</button>
        </form>
    </div>
</main>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script>
    $(function() {
        jQuery.extend(jQuery.validator.messages, {
            required: "Field harus diisi"
        });
        $('.form-daftar').validate({
            errorElement: "div",
            errorPlacement: function(error, element) {
                error.addClass("invalid-feedback");
                error.insertAfter(element);
            },
            highlight: function(element) {
                $(element).removeClass('is-valid').addClass('is-invalid');
            },
            unhighlight: function(element) {
                $(element).removeClass('is-invalid').addClass('is-valid');
            }
        });
    });
</script>