<main class="form-center px-5 py-5">
    <!-- Content here -->
    <div class="text-center">
        <img class="img-fluid mb-3" src="<?= base_url(); ?>/assets/img/logo.png" alt="Image Description">
        <h1 class="fs-3">Login Akun Pendaftaran<br> Mahasiswa Baru <span class="txt-orange">Unmer Malang</span></h1>
    </div>
    <form>
        <div class="form-floating">
            <input type="text" class="form-control" placeholder="No. Pendaftaran" name="nopendaftar">
            <label for="nama">No. Pendaftaran</label>
        </div>
        <div class="form-floating">
            <input type="text" class="form-control" placeholder="Password" name="password">
            <label for="nik">Password</label>
        </div>
        <a href="#" class="txt-white" style="margin-right:8px;">Lupa password?</a>
        <button type="submit" class="btn btn-success" name="login" id="btnSave">Login</button>
        <button type="submit" class="btn btn-success" name="login" id="btnEmail">Google</button>
    </form>

</main>