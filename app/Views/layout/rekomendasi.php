<div class="card no-border">
    <div class="card-header">Rekomendasi</div>
    <div class="card-body p-0">
        <table class="table table-bordered m-0">
            <tbody>
                <tr>
                    <td style="vertical-align:middle; text-align:center"> Nama Yang
                        Merekomendasikan
                    </td>
                    <td colspan="3">
                        <small><b>Saudara Masuk Unmer Malang Karena Rekomendasi dari Siapa ?</b>
                        </small>
                        <br><span id="show">MUHAMMAD FURQON</span>
                    </td>
                </tr>

                <tr>
                    <td style="vertical-align:middle; text-align:center"></td>
                    <td colspan="3">
                        <small><b>Tahu Unmer Malang Dari ?</b> </small>
                        <br> <span id="show">Teman</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>