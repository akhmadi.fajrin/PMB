<!-- tab data keluarga -->
<div class="card no-border">
    <div class="card-header">Wali</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tr id="block-namaayah">
                    <td class="LeftColumnBG">
                        <b id="label-namaayah">
                            Nama Ayah <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">IWAN SETIAWAN</span><span id="edit" style="display:none"><input type="text"
                                name="namaayah" id="namaayah" value="IWAN SETIAWAN" class="form-control" maxlength="50"
                                size="30">
                        </span>
                    </td>
                </tr>
                <tr id="block-kodepropinsilahirayah">
                    <td class="LeftColumnBG">
                        <b id="label-kodepropinsilahirayah">
                            Propinsi Lahir Ayah <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">BANTEN</span>
                    </td>
                </tr>
                <tr>
                    <td class="LeftColumnBG">
                        <b>Kota Lahir Ayah <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">KAB. SERANG</span>
                    </td>
                </tr>
                <tr id="block-tgllahirayah">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-tgllahirayah">
                            Tgl Lahir Ayah <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">27 Nov 1969</span>
                    </td>
                </tr>
                <tr id="block-kodepekerjaanayah">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kodepekerjaanayah">
                            Pekerjaan Ayah <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Wiraswasta</span>
                    </td>
                </tr>
                <tr id="block-kodependidikanayah">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kodependidikanayah">
                            Pendidikan Ayah <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">SMA</span>
                    </td>
                </tr>
                <tr id="block-pendapatanayah">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-pendapatanayah">
                            Pendapatan Ayah</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">4000000</span><span id="edit" style="display:none"><input type="text"
                                name="pendapatanayah" id="pendapatanayah" value="4000000" class="form-control"
                                maxlength="14" size="20">
                        </span>
                    </td>
                </tr>
                <tr id="block-statusayahkandung">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-statusayahkandung">
                            Status <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Kandung</span><span id="edit" style="display:none"><select
                                name="statusayahkandung" id="statusayahkandung" class="form-control">
                                <option value=""></option>
                                <option value="1" selected="">Kandung</option>
                                <option value="2">Tiri</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr id="block-namaibu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-namaibu">
                            Nama Ibu <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">HERMAWATI</span><span id="edit" style="display:none"><input type="text"
                                name="namaibu" id="namaibu" value="HERMAWATI" class="form-control" maxlength="50"
                                size="30">
                        </span>
                    </td>
                </tr>
                <tr id="block-kodepropinsilahiribu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kodepropinsilahiribu">
                            Propinsi Lahir Ibu <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">DKI JAKARTA</span>
                    </td>
                </tr>
                <tr>
                    <td class="LeftColumnBG">
                        <b>Kota Lahir Ibu <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">KOTA JAKARTA TIMUR</span>
                    </td>
                </tr>
                <tr id="block-tgllahiribu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-tgllahiribu">
                            Tgl Lahir Ibu <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">24 Feb 1973</span>
                    </td>
                </tr>
                <tr id="block-kodepekerjaanibu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kodepekerjaanibu">
                            Pekerjaan Ibu <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Ibu Rumah Tangga</span>
                    </td>
                </tr>
                <tr id="block-kodependidikanibu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kodependidikanibu">
                            Pendidikan Ibu <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">D3</span>
                    </td>
                </tr>
                <tr id="block-pendapatanibu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-pendapatanibu">
                            Pendapatan Ibu</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">3000000</span><span id="edit" style="display:none"><input type="text"
                                name="pendapatanibu" id="pendapatanibu" value="3000000" class="form-control"
                                maxlength="14" size="20">
                        </span>
                    </td>
                </tr>
                <tr id="block-statusayahkandung">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-statusayahkandung">
                            Status <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Kandung</span><span id="edit" style="display:none"><select
                                name="statusayahkandung" id="statusayahkandung" class="form-control">
                                <option value=""></option>
                                <option value="1" selected="">Kandung</option>
                                <option value="2">Tiri</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr id="block-kodepropinsiortu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kodepropinsiortu">
                            Propinsi Ortu <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">BANTEN</span>
                    </td>
                </tr>
                <tr>
                    <td class="LeftColumnBG">
                        <b>Kota Ortu <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">KAB. SERANG</span>

                    </td>
                </tr>
                <tr>
                    <td class="LeftColumnBG" style="vertical-align:middle;"><b>Alamat
                            Ortu</b></td>
                    <td class="RightColumnBG">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Jalan :</small>
                                        <p><span id="show">KAMAN SARI</span><span id="edit" style="display:none"><input
                                                    type="text" name="jalanortu" id="jalanortu" value="KAMAN SARI"
                                                    class="form-control" maxlength="150" size="50">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">RT / RW : </small>
                                        <p><span id="show">01</span><span id="edit" style="display:none"><input
                                                    type="text" name="rtortu" id="rtortu" value="01"
                                                    class="form-control" maxlength="5" size="5"
                                                    style="width:15%;display:inline-block;">
                                            </span> / <span id="show">5</span><span id="edit"
                                                style="display:none"><input type="text" name="rwortu" id="rwortu"
                                                    value="5" class="form-control" maxlength="5" size="5"
                                                    style="width:15%;display:inline-block;">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Kelurahan
                                            :</small>
                                        <p><span id="show">CIKANDE</span><span id="edit" style="display:none"><input
                                                    type="text" name="kelortu" id="kelortu" value="CIKANDE"
                                                    class="form-control" maxlength="20" size="50">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Kecamatan
                                            :</small>
                                        <p><span id="show">KEC. CIKANDE</span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr id="block-kodeposortu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kodeposortu">
                            Kode Pos</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">42186</span><span id="edit" style="display:none"><input type="text"
                                name="kodeposortu" id="kodeposortu" value="42186" class="form-control" maxlength="50"
                                size="30">
                        </span>
                    </td>
                </tr>
                <tr id="block-telportu">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-telportu">
                            Telp Ortu</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">081293459048</span><span id="edit" style="display:none"><input type="text"
                                name="telportu" id="telportu" value="081293459048" class="form-control" maxlength="15"
                                size="15">
                        </span>
                    </td>
                </tr>
                <tr id="block-biayadari">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-biayadari">
                            Biaya Sekolah dari</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Orang tua/Wali</span>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="card mt-4 no-border">
        <div class="card-header">Diisi jika salah satu keluarga bekerja di Unmer Malang</div>
        <div class="card-body p-0">
            <table class="table table-bordered m-0">
                <tr id="block-nikkeluarga">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-nikkeluarga">
                            NIK / NRP</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                name="nikkeluarga" id="nikkeluarga" class="form-control" maxlength="30" size="30">
                        </span>
                    </td>
                </tr>

                <tr id="block-namakeluargatni">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-namakeluargatni">
                            Nama Keluarga</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                name="namakeluargatni" id="namakeluargatni" class="form-control" maxlength="50"
                                size="30">
                        </span>
                    </td>
                </tr>

                <tr id="block-satkertni">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-satkertni">
                            Satuan Kerja</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="text" name="satkertni"
                                id="satkertni" class="form-control" maxlength="50" size="30">
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="card mt-4 no-border">
        <div class="card-header">Diisi jika salah satu keluarga sebagai TNI / POLRI</div>
        <div class="card-body p-0">
            <table class="table table-bordered m-0">
                <tr id="block-nikkeluarga">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-nikkeluarga">
                            NIK / NRP</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                name="nikkeluarga" id="nikkeluarga" class="form-control" maxlength="30" size="30">
                        </span>
                    </td>
                </tr>

                <tr id="block-namakeluargatni">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-namakeluargatni">
                            Nama Keluarga</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                name="namakeluargatni" id="namakeluargatni" class="form-control" maxlength="50"
                                size="30">
                        </span>
                    </td>
                </tr>

                <tr id="block-satkertni">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-satkertni">
                            Satuan Kerja</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="text" name="satkertni"
                                id="satkertni" class="form-control" maxlength="50" size="30">
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Kontak Selain Wali</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tr id="block-kontaknama">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kontaknama">
                            Nama Kontak</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">HERAWATI</span><span id="edit" style="display:none"><input type="text"
                                name="kontaknama" id="kontaknama" value="HERAWATI" class="form-control" maxlength="50"
                                size="30">
                        </span>
                    </td>
                </tr>

                <tr id="block-kodepropinsikontak">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kodepropinsikontak">
                            Propinsi kontak</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">DKI JAKARTA</span>
                    </td>
                </tr>

                <tr>
                    <td class="LeftColumnBG">
                        <b>Kota Kontak</b>
                    </td>
                    <td class="RightColumnBG">

                        <span id="edit" style="display:none"><span id="show">3101</span>
                    </td>
                </tr>

                <tr>
                    <td class="LeftColumnBG" style="white-space:nowrap;vertical-align:middle;">
                        <b>Alamat</b>
                    </td>
                    <td class="RightColumnBG">

                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Jalan :</small>
                                        <p><span id="show">JALAN LET JEND SUTOYO</span><span id="edit"
                                                style="display:none"><input type="text" name="jalankontak"
                                                    id="jalankontak" value="JALAN LET JEND SUTOYO" class="form-control"
                                                    maxlength="150" size="50">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">RT / RW :</small>
                                        <p><span id="show">03</span><span id="edit" style="display:none"><input
                                                    type="text" name="rtkontak" id="rtkontak" value="03"
                                                    class="form-control" maxlength="5" size="5"
                                                    style="width:15%;display:inline-block;">
                                            </span> / <span id="show">01</span><span id="edit"
                                                style="display:none"><input type="text" name="rwkontak" id="rwkontak"
                                                    value="01" class="form-control" maxlength="5" size="5"
                                                    style="width:15%;display:inline-block;">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Kelurahan
                                            :</small>
                                        <p><span id="show">CILILITAN</span><span id="edit" style="display:none"><input
                                                    type="text" name="kelkontak" id="kelkontak" value="CILILITAN"
                                                    class="form-control" maxlength="20" size="50">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Kecamatan
                                            :</small>
                                        <p><span id="show">KEC. KRAMAT JATI</span>

                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>

                <tr id="block-kontaktelp">
                    <td class="LeftColumnBG" width="80" style="white-space:nowrap">
                        <b id="label-kontaktelp">
                            Telp Kontak</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">082114536216</span><span id="edit" style="display:none"><input type="text"
                                name="kontaktelp" id="kontaktelp" value="082114536216" class="form-control"
                                maxlength="15" size="15">
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Saudara</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tbody>
                    <tr>
                        <td>No</td>
                        <td>Nama Saudara</td>
                        <td>Tempat Tanggal Lahir</td>
                        <td>Pendidikan</td>
                        <td>Status</td>
                    </tr>

                    <tr>
                        <td>1.</td>
                        <td>M. Fikri Setiadi</td>
                        <td>KAB. SERANG, 01-01-1970</td>
                        <td>SMA</td>
                        <td>Kandung</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>M. Ricky Firmansyah</td>
                        <td>KAB. SERANG, 01-01-1970</td>
                        <td>SMP</td>
                        <td>Kandung</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>