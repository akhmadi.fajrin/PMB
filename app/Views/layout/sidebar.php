<div class="col-md-3">
    <div class="sidebar-menu">
        <ul>
            <li><a href="#" class="profil-anda">Profil Anda</a></li>
            <li><a href="#" class="tagihan">Tagihan Pendaftar</a></li>
            <li><a href="#" class="cetak-kartu">Cetak Kartu Ujian</a></li>
            <li><a href="#" class="cetak-formulir">Cetak Formulir Pendaftaran</a></li>
            <li><a href="#" class="ganti-pass">Ganti Password</a></li>
            <li><a href="#" class="pengumuman current-page">Pengumuman Kelulusan</a></li>
        </ul>
    </div>
</div>