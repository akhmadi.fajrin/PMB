<div class="card no-border">
    <div class="card-header">Kemampuan Bahasa</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tbody>
                    <tr id="block-bhsarab">
                        <td class="LeftColumnBG">
                            <b id="label-bhsarab">
                                Bahasa Arab</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">Tidak Bisa</span>
                        </td>
                    </tr>
                    <tr id="block-bhsinggris">
                        <td class="LeftColumnBG">
                            <b id="label-bhsinggris">
                                Bahasa Inggris</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">Tidak Bisa</span>
                        </td>
                    </tr>
                    <tr id="block-pengkomp">
                        <td class="LeftColumnBG">
                            <b id="label-pengkomp">
                                Komputer</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">Tidak Bisa</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Riwayat Pendidikan Formal</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tbody>
                    <tr>
                        <td>No</td>
                        <td>Nama Jenis Pendidikan</td>
                        <td>Tempat Pendidikan</td>
                        <td>Tahun Masuk</td>
                        <td>Tahun Lulus</td>
                    </tr>

                    <tr>
                        <td>1.</td>
                        <td>SD</td>
                        <td>Serang</td>
                        <td>2009</td>
                        <td>2015</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>SMP</td>
                        <td>Serang</td>
                        <td>2015</td>
                        <td>2018</td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>SMA</td>
                        <td>Serang</td>
                        <td>2021</td>
                        <td>2021</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Riwayat Pendidikan Non Formal/Pelatihan, Kursus yg pernah Diikuti
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tbody>
                    <tr>
                        <td>No</td>
                        <td>Nama Jenis Pelatihan</td>
                        <td>Tingkat</td>
                        <td>Tahun</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Pengalaman Organisasi</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tbody>
                    <tr>
                        <td>No</td>
                        <td>Nama Organisasi</td>
                        <td>Jabatan</td>
                        <td>Tahun</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Prestasi Akademik</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tbody>
                    <tr>
                        <td>No</td>
                        <td>Nama Prestasi Yang Diraih</td>
                        <td>Juara</td>
                        <td>Tingkat</td>
                        <td>Tahun</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Prestasi Non Akademik</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tbody>
                    <tr>
                        <td>No</td>
                        <td>Nama Prestasi Yang Diraih</td>
                        <td>Juara</td>
                        <td>Tingkat</td>
                        <td>Tahun</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>