<div class="card data-input">
    <h5 class="card-header">
        Biodata
    </h5>
    <div class="card-body">
        <div class="row">

            <div class="input-container">
                <div class="col-25">
                    <label for="label-kodewn">Kewarganegaraan</label>
                </div>
                <div class="col-75">
                    <select id="kodewn" name="kodewn" required class="form-control">
                        <option selected>WNI</option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="kk">Nomor KK</label>
                </div>
                <div class="col-75">
                    <input type="text" name="kk" required class="form-control">
                </div>
            </div>

            <div class="input-container">
                <div class="col-25">
                    <label for="sex">Jenis Kelamin</label>
                </div>
                <div class="col-75">
                    <select id="sex" name="sex" class="form-control width-50" required>
                        <option value="L">Laki - Laki</option>
                        <option value="P">Perempuan</option>
                        <option selected>-- Pilih Jenis Kelamin --</option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="tmplahir">Tempat Lahir</label>
                </div>
                <div class="col-75">
                    <input id="tmplahir" type="text" name="tmplahir" class="form-control" required>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="tgllahir">Tanggal Lahir</label>
                </div>
                <div class="col-75">
                    <input id="tgllahir" type="date" class="form-control" name="tgllahir" id required>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label>
                        <h4>Alamat Lengkap</h4>
                    </label>
                </div>
                <div class="col-75 sub-label">
                    <label for="alamat">Jalan</label>
                    <input id="jalan" type="text" name="jalan" placeholder="--Nama Jalan, Dusun/Desa--"
                        class="form-control" required>
                    <label for="rtrw">RT/RW</label><br>
                    <input type="text" name="rt" id="rt" class="form-control" maxlength="5"
                        style="width:15%;display:inline-block; margin-right: 8px;" class="form-control">
                    <input type="text" name="rw" id="rw" class="form-control" maxlength="5"
                        style="width:15%;display:inline-block;" class="form-control"><br>
                    <label>Cari Desa / Kelurahan (Bila tidak ada silahkan isi secara manual):</label>
                    <select name="#" id=""></select>
                </div>
            </div>

            <div class="input-container">
                <div class="col-25">
                    <label>
                        <h4>Wilayah Administrasi</h4>
                    </label>
                </div>
                <div class="col-75 sub-label">
                    <label for="kodepropinsi">Propinsi</label>
                    <select id="kodepropinsi" name="kodepropinsi" class="form-control" required>
                        <option selected>--Pilih Propinsi Terlebih Dahulu--</option>
                    </select>
                    <label for="kodekota">Kota</label>
                    <select id="kodekota" name="kodekota" class="form-control" required>
                        <option selected>--Pilih Kota--</option>
                    </select>
                    <label for="kec">Kecamatan</label>
                    <select id="kec" name="kec" class="form-control" required>
                        <option selected>--Pilih Kecamatan</option>
                    </select>
                    <label for="kel">Kelurahan</label>
                    <select id="kel" name="kel" class="form-control" required>
                        <option selected>--Pilih Kecamatan</option>
                    </select>
                    <br>
                </div>
            </div>

            <!-- <div class="input-container">
                <div class="col-25">
                    <label for="kodepos">Kode Pos</label>
                </div>
                <div class="col-75">
                    <input type="text" name="kodepos" required>
                </div>
            </div> -->

            <!-- <div class="input-container">
                <div class="col-25">
                    <label>Handphone</label>
                </div>
                <div class="col-75"><input type="number" name="hp" required></div>
            </div>

            <div class="input-container">
                <div class="col-25">
                    <label for="hp">WhatsApp</label>
                </div>
                <div class="col-75">
                    <input type="number" name="hp" required>
                </div>
            </div> -->

            <!-- <div class="input-container">
                <div class="col-25">
                    <label for="hp2">SMS Broadcast</label>
                </div>
                <div class="col-75">
                    <input type="number" name="hp2" required>
                </div>
            </div> -->

            <!-- <div class="input-container">
                <div class="col-25">
                    <label for="email">E-Mail Aktif</label>
                </div>
                <div class="col-75">
                    <input type="text" name="email" required>
                </div>
            </div> -->

            <!-- <div class="input-container">
                <div class="col-25">
                    <label for="mhsasing">Mahasiswa Asing ?</label>
                </div>
                <div class="col-75">
                    <select id="mhsasing" name="mhsasing">
                        <option value="1">Ya</option>
                        <option value="2">Tidak</option>
                        <option selected>--Pilih--</option>
                    </select>
                </div>
            </div> -->

            <div class="input-container">
                <div class="col-25">
                    <label for="statusnikah">Status Nikah</label>
                </div>
                <div class="col-75">
                    <select id="statusnikah" name="statusnikah" class="form-control width-50" required>
                        <option selected>Islam</option>
                    </select>
                </div>
            </div>

            <div class="input-container">
                <div class="col-25">
                    <label for="kodeagama">Agama</label>
                </div>
                <div class="col-75">
                    <select id="kodeagama" name="kodeagama" class="form-control width-50" required>
                        <option selected>Islam</option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="ukuranalmamater">Ukuran Jas Almamater</label>
                </div>
                <div class="col-75">
                    <select id="ukuranalmamater" name="ukuranalmamater" class="form-control width-50" required>
                        <option selected>--Pilih Ukuran</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>