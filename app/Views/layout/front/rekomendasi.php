<!-- Tab Data Keluarga -->

<div class="card data-input">
    <h5 class="card-header">
        Rekomendasi
    </h5>
    <div class="card-body">
        <div class="row">
            <strong>Saudara Masuk Unmer Malang Karena Rekomendasi dari Siapa ?</strong>
            <div class="col-25">
                <label for="namarekom" name="namarekom">Nama Yang Merekomendasikan</label>
            </div>
            <div class="col-75">
                <input type="text" name="namarekom" class="form-control">
            </div>
            <div class="col-25">
                <label for="tahuunmerdari">Tahu Unmer Malang dari ?</label>
            </div>
            <div class="col-75">
                <select id="tahuunmerdari" name="tahuunmerdari" class="form-control">
                    <option value="1">Brosur</option>
                    <option value="2">Website</option>
                    <option value="3">Alumni</option>
                    <option selected></option>
                </select>
            </div>
        </div>
    </div>
</div>