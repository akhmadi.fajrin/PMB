<div class="card data-input">
    <div class="card-header">Asal Sekolah</div>
    <div class="card-body">
        <div class="row">
            <div class="input-container">
                <div class="col-25"><label for="">Cari Sekolah</label></div>
                <div class="col-75"><select name="#" id="" class="form-control"></select></div>
            </div>
            <div class="input-container">
                <div class="col-25"><label for="xasalsmu">Nama Sekolah</label></div>
                <div class="col-75"><input id="xasalsmu" name="xasalsmu" type="text" class="form-control" required>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25"><label for="">Propinsi Sekolah</label></div>
                <div class="col-75"><select name="propinsismu" id="propinsismu" class="form-control" required></select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25"><label for="">Kota Sekolah</label></div>
                <div class="col-75"><select name="kodekotasmu" id="kodekotasmu" class="form-control" required></select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25"><label for="kodejenissmu">Jenis Sekolah</label></div>
                <div class="col-75"><select name="kodejenissmu" id="kodejenissmu" class="form-control"
                        required></select></div>
            </div>

            <div class="input-container">
                <div class="col-25"><label for="alamatsmu">Alamat Sekolah</label></div>
                <div class="col-75"><input name="alamatsmu" id="alamatsmu" type="text" class="form-control" required>
                </div>
            </div>

            <div class="input-container">
                <div class="col-25"><label for="telpsmu">Telp Sekolah</label></div>
                <div class="col-75"><input id="telpsmu" name="telpsmu" type="number" class="form-control"></div>
            </div>
            <div class="input-container">
                <div class="col-25"><label for="thnmasuksmaasal">Tahun Masuk</label></div>
                <div class="col-75"><input id="thnmasuksmaasal" name="thnmasuksmaasal" type="number"
                        class="form-control"></div>
            </div>

            <div class="input-container">
                <div class="col-25"><label for="thnlulussmaasal">Tahun Lulus</label></div>
                <div class="col-75"><input id="thnlulussmaasal" name="thnlulussmaasal" type="number"
                        class="form-control"></div>
            </div>

            <div class="input-container">
                <div style="text-align: center; margin-top: 15px; margin-bottom: 10px;">
                    <label style="font-size: 18px;">Nilai Rapot</label>
                </div>
                <table width="100%">
                    <tbody>
                        <tr>
                            <td colspan="2" class="text-center" style="padding-right:10px;"><b>Kelas X</b></td>
                            <td colspan="2" class="text-center" style="padding-right:10px;"><b>Kelas XI</b></td>
                            <td class="text-center" style="padding-right:10px;"><b>Kelas XII</b></td>
                        </tr>
                        <tr>
                            <td class="text-center" style="padding-right:10px;">Smt. 1</td>
                            <td class="text-center" style="padding-right:10px;">Smt. 2</td>
                            <td class="text-center" style="padding-right:10px;">Smt. 1</td>
                            <td class="text-center" style="padding-right:10px;">Smt. 2</td>
                            <td class="text-center" style="padding-right:10px;">Smt. 1</td>
                        </tr>
                        <tr>
                            <td><span id="show" style="display: none;"></span><span id="edit"><input type="text"
                                        name="raport_10_1" id="raport_10_1" class="form-control number" maxlength="10"
                                        size="50" style="width:90%;text-align:center;" placeholder="100.00"
                                        data-decimal="2" data-max="100">
                                </span></td>
                            <td><span id="show" style="display: none;"></span><span id="edit"><input type="text"
                                        name="raport_10_2" id="raport_10_2" class="form-control number" maxlength="10"
                                        size="50" style="width:90%;text-align:center;" placeholder="100.00"
                                        data-decimal="2" data-max="100">
                                </span></td>
                            <td><span id="show" style="display: none;"></span><span id="edit"><input type="text"
                                        name="raport_11_1" id="raport_11_1" class="form-control number" maxlength="10"
                                        size="50" style="width:90%;text-align:center;" placeholder="100.00"
                                        data-decimal="2" data-max="100">
                                </span></td>
                            <td><span id="show" style="display: none;"></span><span id="edit"><input type="text"
                                        name="raport_11_2" id="raport_11_2" class="form-control number" maxlength="10"
                                        size="50" style="width:90%;text-align:center;" placeholder="100.00"
                                        data-decimal="2" data-max="100">
                                </span></td>
                            <td><span id="show" style="display: none;"></span><span id="edit"><input type="text"
                                        name="raport_12_1" id="raport_12_1" class="form-control number" maxlength="10"
                                        size="50" style="width:90%;text-align:center;" placeholder="100.00"
                                        data-decimal="2" data-max="100">
                                </span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- <div class="input-container">
                <div class="col-25"><label for="">NISN</label></div>
                <div class="col-75"><input type="number" class="form-control"></div>
            </div> -->
        </div>
    </div>
</div>

<div class="card data-input">
    <div class="card-header">Informasi Mahasiswa Transfer</div>
    <div class="card-body">
        <div class="input-container">
            <div class="col-25">
                <label for="ptasal"> Universitas Asal</label>
            </div>
            <div class="col-75">
                <input id="ptasal" name="ptasal" type="text" class="form-control">
            </div>
        </div>

        <div class="input-container">
            <div class="col-25">
                <label for="nimptasal">NIM Universitas Asal</label>
            </div>
            <div class="col-75">
                <input id="nimptasal" name="nimptasal" type="text" class="form-control">
            </div>
        </div>

        <div class="input-container">
            <div class="col-25">
                <label for="propinsiptasal">Propinsi Universitas</label>
            </div>
            <div class="col-75">
                <input id="propinsiptasal" name="propinsiptasal" type="text" class="form-control">
            </div>
        </div>

        <div class="input-container">
            <div class="col-25">
                <label for="ptjurusan">Prodi Asal</label>
            </div>
            <div class="col-75">
                <input id="ptjurusan" name="ptjurusan" type="text" class="form-control">
            </div>
        </div>

        <div class="input-container">
            <div class="col-25">
                <label for="ptthnlulus">Tahun Lulus</label>
            </div>
            <div class="col-75">
                <input id="ptthnlulus" name="ptthnlulus" type="text" class="form-control">
            </div>
        </div>

        <div class="input-container">
            <div class="col-25">
                <label for="ptipk">IPK</label>
            </div>
            <div class="col-75">
                <input id="ptipk" name="ptipk" type="text" class="form-control">
            </div>
        </div>

    </div>
</div>
<div class="card data-input mt-4">
    <div class="card-header">Pondok Pesantren </div>
    <div class="card-body">
        <div class="input-container">
            <div class="col-25">
                <label>Pernah Belajar <br>di Ponpes ?</label>
            </div>
            <div class="col-75">
                <input type="radio" name="pernahponpes" id="pernahponpes_1" value="1" onchange="cekPonPes();">
                <label for="_1" onchange="cekPonPes();">Ya</label>
                <input type="radio" name="pernahponpes" id="pernahponpes_0" value="0" checked="checked"
                    onchange="cekPonPes();"><label for="_0" onchange="cekPonPes();">Tidak</label>
                </span>
            </div>
        </div>

        <div class="input-container" id="block-namaponpes">
            <div class="col-25">
                <label for="namaponpes">Nama Pesantren</label>
            </div>
            <div class="col-75">
                <input id="namaponpes" name="namaponpes" type="text" class="form-control">
            </div>
        </div>

        <div class="input-container" id="block-alamatponpes">
            <div class="col-25">
                <label for="alamatponpes">Alamat Pesantren</label>
            </div>
            <div class="col-75">
                <input id="alamatponpes" name="alamatponpes" type="text" class="form-control">
            </div>
        </div>

        <div class="input-container" id="block-propinsiponpes">
            <div class="col-25">
                <label for="propinsiponpes">Propinsi Pesantren</label>
            </div>
            <div class="col-75">
                <select id="propinsiponpes" name="propinsiponpes" class="form-control"></select>
            </div>
        </div>

        <div class="input-container" id="block-kodekotaponpes">
            <div class="col-25">
                <label for="kodekotaponpes">Kota Pesantren</label>
            </div>
            <div class="col-75">
                <select id="kodekotaponpes" name="kodekotaponpes" class="form-control"></select>
            </div>
        </div>

        <div class="input-container" id="block-lamaponpes">
            <div class="col-25">
                <label for="lamaponpes">Lama Belajar</label>
            </div>
            <div class="col-75">
                <input id="lamaponpes" name="lamaponpes" type="text" class="form-control">
            </div>
        </div>
    </div>
</div>