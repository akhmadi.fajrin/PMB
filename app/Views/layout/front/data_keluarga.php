<!-- Tab Data Keluarga -->

<div class="card data-input">
    <h5 class="card-header">
        Orang Tua
    </h5>
    <div class="card-body">
        <div class="row">
            <div class="input-container">
                <div class="col-25">
                    <label for="namaayah">Nama Ayah</label>
                </div>
                <div class="col-75">
                    <input type="text" name="namaayah" class="form-control" required>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="kodepekerjaanayah">Pekerjaan Ayah</label>
                </div>
                <div class="col-75">
                    <select id="kodepekerjaanayah" name="kodepekerjaanayah" class="form-control">
                        <option value="1">PNS</option>
                        <option value="2">SWASTA</option>
                        <option value="3">TNI/POLRI</option>
                        <option selected></option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="kodependidikanayah">Pendidikan Ayah</label>
                </div>
                <div class="col-75">
                    <select id="kodependidikanayah" name="kodependidikanayah">
                        <option value="1">SD</option>
                        <option value="1">SMP</option>
                        <option value="1">SMA</option>
                        <option value="1">S1</option>
                        <option value="1">S2</option>
                        <option value="1">S3</option>
                        <option selected></option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="kodependapatanayah">Pendapatan Ayah</label>
                </div>
                <div class="col-75">
                    <select id="kodependapatanayah" name="kodependapatanayah">
                        <option value="1">
                            < 1.000.000</option>
                        <option value="1">1.000.000 - 2.000.000</option>
                        <option value="1">> 2.000.000 - 3.000.000</option>
                        <option value="1">> 3.000.000</option>
                        <option selected>--</option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="namaibu" name="namaibu">Nama Ibu</label>
                </div>
                <div class="col-75">
                    <input type="text" name="namaibu" required class="form-control">
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="kodepekerjaanibu">Pekerjaan Ibu</label>
                </div>
                <div class="col-75">
                    <select id="kodepekerjaanibu" name="kodepekerjaanibu" class="form-control">
                        <option value="1">PNS</option>
                        <option value="2">SWASTA</option>
                        <option value="3">TNI/POLRI</option>
                        <option selected></option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="kodependidikanibu">Pendidikan Ayah</label>
                </div>
                <div class="col-75">
                    <select id="kodependidikanibu" name="kodependidikanibu">
                        <option value="1">SD</option>
                        <option value="1">SMP</option>
                        <option value="1">SMA</option>
                        <option value="1">S1</option>
                        <option value="1">S2</option>
                        <option value="1">S3</option>
                        <option selected></option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="kodependapatanibu">Pendapatan Ibu</label>
                </div>
                <div class="col-75">
                    <select id="kodependapatanibu" name="kodependapatanibu">
                        <option value="1">
                            < 1.000.000</option>
                        <option value="1">1.000.000 - 2.000.000</option>
                        <option value="1">> 2.000.000 - 3.000.000</option>
                        <option value="1">> 3.000.000</option>
                        <option selected>--</option>
                    </select>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label>
                        <h4>Alamat Lengkap</h4>
                    </label>
                </div>
                <div class="col-75 sub-label">
                    <label for="alamat">Jalan</label>
                    <input id="jalanortu" type="text" name="jalanortu" placeholder="--Nama Jalan, Dusun/Desa--" required
                        class="form-control">
                    <label for="rtrw">RT/RW</label><br>
                    <input type="text" name="rtortu" id="rtortu" class="form-control" maxlength="5"
                        style="width:15%;display:inline-block; margin-right: 8px;" class="form-control">
                    <input type="text" name="rwortu" id="rwortu" class="form-control" maxlength="5"
                        style="width:15%;display:inline-block;" class="form-control"><br>
                    <label>Cari Desa / Kelurahan (Bila tidak ada silahkan isi secara manual):</label>
                    <select name="#" id=""></select>
                </div>
            </div>

            <div class="input-container">
                <div class="col-25">
                    <label>
                        <h4>Wilayah Administrasi</h4>
                    </label>
                </div>
                <div class="col-75 sub-label">
                    <label for="kodepropinsiortu">Propinsi</label>
                    <select id="kodepropinsiortu" name="kodepropinsiortu" required class="form-control">
                        <option selected>--Pilih Propinsi Terlebih Dahulu--</option>
                    </select>
                    <label for="kodekotaortu">Kota</label>
                    <select id="kodekotaortu" name="kodekotaortu" required class="form-control">
                        <option selected>--Pilih Kota--</option>
                    </select>
                    <label for="kecortu">Kecamatan</label>
                    <select id="kecortu" name="kecortu" required class="form-control">
                        <option selected>--Pilih Kecamatan</option>
                    </select>
                    <label for="kelortu">Kelurahan</label>
                    <select id="kelortu" name="kelortu" required class="form-control">
                        <option selected>--Pilih Kecamatan</option>
                    </select>
                    <br>
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="telportu">Telp Ortu</label>
                </div>
                <div class="col-75">
                    <input id="telportu" type="number" name="telportu" required class="form-control">
                </div>
            </div>
            <div class="input-container">
                <div class="col-25">
                    <label for="biayadari">Biaya Sekolah dari</label>
                </div>
                <div class="col-75">
                    <select id="biayadari" name="biayadari" class="form-control">
                        <option value="1">Beasiswa</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card data-input mt-4">
    <div class="card-header">Wali</div>
    <div class="card-body">
        <div class="input-container">
            <div class="col-25">
                <label for="kontaknama">Nama Wali</label>
            </div>
            <div class="col-75">
                <input id="kontaknama" name="kontaknama" type="text">
            </div>
        </div>

        <div class="input-container">
            <div class="col-25">
                <label>
                    <h4>Alamat Lengkap</h4>
                </label>
            </div>
            <div class="col-75 sub-label">
                <label for="alamat">Jalan</label>
                <input type="text" id="jalankontak" name="jalankontak" placeholder="--Nama Jalan, Dusun/Desa--"
                    class="form-control">
                <label for="rtrw">RT/RW</label><br>
                <input type="text" name="rtkontak" id="rtkontak" class="form-control" maxlength="5"
                    style="width:15%;display:inline-block; margin-right: 8px;" class="form-control">
                <input type="text" name="rwkontak" id="rwkontak" class="form-control" maxlength="5"
                    style="width:15%;display:inline-block;" class="form-control"><br>
                <label>Cari Desa / Kelurahan (Bila tidak ada silahkan isi secara manual):</label>
                <select name="#" id=""></select>
            </div>
        </div>

        <div class="input-container">
            <div class="col-25">
                <label>
                    <h4>Wilayah Administrasi</h4>
                </label>
            </div>
            <div class="col-75 sub-label">
                <label for="kodepropinsikontak">Propinsi</label>
                <select id="kodepropinsikontak" name="kodepropinsikontak" class="form-control">
                    <option selected>--Pilih Propinsi Terlebih Dahulu--</option>
                </select>
                <label for="kodekotakotak">Kota</label>
                <select id="kodekotakotak" name="kodekotakotak" class="form-control">
                    <option selected>--Pilih Kota--</option>
                </select>
                <label for="kodekotakotak">Kecamatan</label>
                <select id="kodekotakotak" name="kodekotakotak" class="form-control">
                    <option selected>--Pilih Kecamatan</option>
                </select>
                <label for="kelkontak">Kelurahan</label>
                <select id="kelkontak" name="kelkontak" class="form-control">
                    <option selected>--Pilih Kecamatan</option>
                </select>
                <br>
            </div>
        </div>

        <div class="input-container">
            <div class="col-25">
                <label for="biaya">Telepon Wali</label>
            </div>
            <div class="col-75">
                <input type="text">
            </div>
        </div>

    </div>
</div>