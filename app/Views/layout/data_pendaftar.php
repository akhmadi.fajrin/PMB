<div class="card no-border">
    <div class="card-header">Biodata</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tr id="block-sex">
                    <td class="LeftColumnBG">
                        <b id="label-sex">
                            Jenis Kelamin <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td colspan="3">
                        <span id="show">Laki-Laki</span><span id="edit" style="display:none"><select name="sex" id="sex"
                                class="form-control">
                                <option value="L" selected="">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr id="block-tmplahir">
                    <td class="LeftColumnBG">
                        <b id="label-tmplahir">
                            Tempat Lahir <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">SERANG</span><span id="edit" style="display:none"><input type="text"
                                name="tmplahir" id="tmplahir" value="SERANG" class="form-control" maxlength="50"
                                size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-tgllahir">
                    <td class="LeftColumnBG">
                        <b id="label-tgllahir">
                            Tanggal Lahir <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">2 Agustus 2002</span><span id="edit" style="display:none"><input type="text"
                                name="tgllahir" id="tgllahir" value="02-08-2002" class="form-control" maxlength="10"
                                size="10" style="width:50%;display:inline-block; margin-right:8px;" readonly="readonly">
                            <img class="btn btn-warning" src="images/cal.png" id="tgllahir_trg" style="cursor:pointer;"
                                title="Pilih Tgl Lahir"> &lt;&lt; <i style="font-size:11px">klik untuk pilih tanggal</i>
                            <script type="text/javascript">
                            Calendar.setup({
                                inputField: "tgllahir",
                                ifFormat: "%d-%m-%Y",
                                button: "tgllahir_trg",
                                align: "Br",
                                singleClick: true
                            });
                            </script>

                        </span>
                    </td>
                </tr>
                <tr id="block-goldarah">
                    <td class="LeftColumnBG">
                        <b id="label-goldarah">
                            Gol Darah</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">O</span><span id="edit" style="display:none"><select name="goldarah"
                                id="goldarah" class="form-control">
                                <option value=""></option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="AB">AB</option>
                                <option value="O" selected="">O</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="LeftColumnBG" style="white-space:nowrap; vertical-align:middle;">
                        <b>Alamat</b>
                    </td>
                    <td class="RightColumnBG">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Jalan:</small>
                                        <p><span id="show">KAMAN SARI</span><span id="edit" style="display:none"><input
                                                    type="text" name="jalan" id="jalan" value="KAMAN SARI"
                                                    class="form-control" maxlength="150" size="50">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">RT/ RW: </small>
                                        <p><span id="show">1</span><span id="edit" style="display:none"><input
                                                    type="text" name="rt" id="rt" value="1" class="form-control"
                                                    maxlength="5" size="5" style="width:15%;display:inline-block;">
                                            </span> / <span id="show">5</span><span id="edit"
                                                style="display:none"><input type="text" name="rw" id="rw" value="5"
                                                    class="form-control" maxlength="5" size="5"
                                                    style="width:15%;display:inline-block;">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Kelurahan: </small>
                                        <p><span id="show">CIKANDE</span>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Kecamatan: </small>
                                        <p><span id="show">KEC. CIKANDE</span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr id="block-kodepropinsi">
                    <td class="LeftColumnBG">
                        <b id="label-kodepropinsi">
                            Provinsi <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">BANTEN</span>
                    </td>
                </tr>
                <tr id="block-kodepos">
                    <td class="LeftColumnBG">
                        <b id="label-kodepos">
                            KodePos <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">42186</span><span id="edit" style="display:none"><input type="text"
                                name="kodepos" id="kodepos" value="42186" class="form-control" maxlength="150"
                                size="50">
                        </span>
                    </td>
                </tr>

                <tr>
                    <td class="LeftColumnBG" style="white-space:nowrap;vertical-align:middle;"><b>No.
                            Telpon</b>
                    </td>
                    <td class="RightColumnBG">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">No. Telp <span id="edit"
                                                style="display:none">*</span> :</small>
                                        <p><span id="show">081293459048</span><span id="edit"
                                                style="display:none"><input type="text" name="telp" id="telp"
                                                    value="081293459048" class="form-control" maxlength="15" size="15">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">No. Telp(2) :</small>
                                        <p><span id="show"></span><span id="edit" style="display:none"><input
                                                    type="text" name="telp2" id="telp2" class="form-control"
                                                    maxlength="15" size="15">
                                            </span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="LeftColumnBG" style="white-space:nowrap;vertical-align:middle;"><b>No.
                            Handphone</b></td>
                    <td class="RightColumnBG">
                        <table width="50%">
                            <tbody>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">No. <span class="text-uppercase">Hp <span
                                                    id="edit" style="display:none">*</span></span> :</small>
                                        <p><span id="show">089650543728</span><span id="edit"
                                                style="display:none"><input type="text" name="hp" id="hp"
                                                    value="089650543728" class="form-control" maxlength="15" size="15">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">No. <span
                                                class="text-uppercase">Hp(2)</span>
                                            :</small>
                                        <p><span id="show">089650543728</span><span id="edit"
                                                style="display:none"><input type="text" name="hp2" id="hp2"
                                                    value="089650543728" class="form-control" maxlength="15" size="15">
                                            </span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="LeftColumnBG" style="white-space:nowrap"><b>E-Mail</b></td>
                    <td class="RightColumnBG">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Email :</small>
                                        <p><span id="show">rzkmln0571@gmail.com</span><span id="edit"
                                                style="display:none"><input type="text" name="email" id="email"
                                                    value="rzkmln0571@gmail.com" class="form-control" maxlength="50"
                                                    size="30">
                                            </span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="text-smooth text-block">Email(2) :</small>
                                        <p><span id="show">hermasetiawan2402Qgmail.com</span><span id="edit"
                                                style="display:none"><input type="text" name="email2" id="email2"
                                                    value="hermasetiawan2402Qgmail.com" class="form-control"
                                                    maxlength="50" size="30">
                                            </span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr id="block-isasing">
                    <td class="LeftColumnBG">
                        <b id="label-isasing">
                            Mahasiswa Asing ?</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Tidak</span><span id="edit" style="display:none"><input type="radio"
                                name="isasing" id="isasing_1" value="1"><label for="_1">Ya</label> <input type="radio"
                                name="isasing" id="isasing_0" value="0" checked=""><label for="_0">Tidak</label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="LeftColumnBG" style="white-space:nowrap"><b>Status Nikah</b></td>
                    <td class="RightColumnBG">
                        <span id="show">Belum Menikah</span><span id="edit" style="display:none"><select
                                name="statusnikah" id="statusnikah" class="form-control">
                                <option value="0" selected="">Belum Menikah</option>
                                <option value="3">Duda</option>
                                <option value="2">Janda</option>
                                <option value="1">Menikah</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr id="block-kodeagama">
                    <td class="LeftColumnBG">
                        <b id="label-kodeagama">
                            Agama <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Islam</span><span id="edit" style="display:none"><select name="kodeagama"
                                id="kodeagama" class="form-control">
                                <option value="1" selected="">Islam</option>
                                <option value="2">Katolik</option>
                                <option value="3">Hindu</option>
                                <option value="4">Budha</option>
                                <option value="5">Kristen</option>
                                <option value="6">Konghucu</option>
                                <option value="7">Lainnya</option>
                                <option value="8">Tdk Diisi</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr id="block-kodewn">
                    <td class="LeftColumnBG">
                        <b id="label-kodewn">
                            Kewarganegaraan <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">WNI</span><span id="edit" style="display:none"><select name="kodewn" id="kodewn"
                                class="form-control">
                                <option value="WNI" selected="">WNI</option>
                                <option value="WNA">WNA</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr id="block-nomorktp">
                    <td class="LeftColumnBG">
                        <b id="label-nomorktp">
                            Nomor KTP <span id="edit" style="display:none">*</span></b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">3604150208020001</span><span id="edit" style="display:none"><input type="text"
                                name="nomorktp" id="nomorktp" value="3604150208020001" class="form-control"
                                maxlength="35" size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-nomorpaspor">
                    <td class="LeftColumnBG">
                        <b id="label-nomorpaspor">
                            Nomor Paspor</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">-</span><span id="edit" style="display:none"><input type="text"
                                name="nomorpaspor" id="nomorpaspor" value="-" class="form-control" maxlength="35"
                                size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-nomorvisa">
                    <td class="LeftColumnBG">
                        <b id="label-nomorvisa">
                            Nomor Visa</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">-</span><span id="edit" style="display:none"><input type="text" name="nomorvisa"
                                id="nomorvisa" value="-" class="form-control" maxlength="35" size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-nomorkk">
                    <td class="LeftColumnBG">
                        <b id="label-nomorkk">
                            Nomor KK</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">3604150405090018</span><span id="edit" style="display:none"><input type="text"
                                name="nomorkk" id="nomorkk" value="3604150405090018" class="form-control" maxlength="35"
                                size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-filepassport">
                    <td class="LeftColumnBG">
                        <b id="label-filepassport">
                            File Paspor</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="file"
                                name="filepassport" id="filepassport" size="10" class="form-control">
                        </span>
                    </td>
                </tr>
                <tr id="block-filevisa">
                    <td class="LeftColumnBG">
                        <b id="label-filevisa">
                            File Visa</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="file" name="filevisa"
                                id="filevisa" size="10" class="form-control">
                        </span>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>
<div class="card no-border mt-4">
    <div class="card-header">Data Tambahan</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tr>
                    <td><b>Anak Ke-</b></td>
                    <td><span id=" show">2</span><span id="edit" style="display:none"><input type="text" name="anakke"
                                id="anakke" value="2" class="form-control" maxlength="2" size="5"
                                style="border-radius:4px; height:34px; width:25%; border:1px solid #bbb; display:inline;">
                        </span> dari <span id="show">3</span><span id="edit" style="display:none"><input type="text"
                                name="daribrpsaudara" id="daribrpsaudara" value="3" class="form-control" maxlength="2"
                                size="5"
                                style="border-radius:4px; height:34px;width:25%; border:1px solid #bbb;display:inline;">
                        </span> bersaudara
                    </td>
                </tr>
                <tr id="block-kodepekerjaan">
                    <td class="LeftColumnBG">
                        <b id="label-kodepekerjaan">
                            Pekerjaan</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Lain-lain</span><span id="edit" style="display:none"><select
                                name="kodepekerjaan" id="kodepekerjaan" class="form-control">
                                <option value="1">PNS</option>
                                <option value="10">Dosen UNMER</option>
                                <option value="11">Pensiun</option>
                                <option value="2">Swasta</option>
                                <option value="3">Petani</option>
                                <option value="4">Polri</option>
                                <option value="5">Wiraswasta</option>
                                <option value="6">Ibu Rumah Tangga</option>
                                <option value="7">TNI</option>
                                <option value="8">Polisi</option>
                                <option value="9">Karyawan UNMER</option>
                                <option value="99" selected="">Lain-lain</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr id="block-jarakrumah">
                    <td class="LeftColumnBG">
                        <b id="label-jarakrumah">
                            Jarak Rumah ke Kampus (km)</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="text" name="jarakrumah"
                                id="jarakrumah" class="form-control" maxlength="10" size="10"
                                style="width:25%;display:inline-block;">
                        </span>
                    </td>
                </tr>
                <tr id="block-transportasi">
                    <td class="LeftColumnBG">
                        <b id="label-transportasi">
                            Transportasi yg digunakan</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">umum</span><span id="edit" style="display:none"><input type="text"
                                name="transportasi" id="transportasi" value="umum" class="form-control" maxlength="100"
                                size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-hoby">
                    <td class="LeftColumnBG">
                        <b id="label-hoby">
                            Hobi</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show"></span><span id="edit" style="display:none"><input type="text" name="hoby"
                                id="hoby" class="form-control" maxlength="50" size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-cita2">
                    <td class="LeftColumnBG">
                        <b id="label-cita2">
                            Cita-cita</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Hakim</span><span id="edit" style="display:none"><input type="text" name="cita2"
                                id="cita2" value="Hakim" class="form-control" maxlength="50" size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-nohpteman">
                    <td class="LeftColumnBG">
                        <b id="label-nohpteman">
                            No. HP Teman</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">081381784001</span><span id="edit" style="display:none"><input type="text"
                                name="nohpteman" id="nohpteman" value="081381784001" class="form-control" maxlength="20"
                                size="50">
                        </span>
                    </td>
                </tr>
                <tr id="block-ukuranalmamater">
                    <td class="LeftColumnBG">
                        <b id="label-ukuranalmamater">
                            Ukuran Jas Almamater</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">M</span><span id="edit" style="display:none"><select name="ukuranalmamater"
                                id="ukuranalmamater" class="form-control">
                                <option value=""></option>
                                <option value="XXXS">XXXS</option>
                                <option value="XXS">XXS</option>
                                <option value="XS">XS</option>
                                <option value="S">S</option>
                                <option value="M" selected="">M</option>
                                <option value="L">L</option>
                                <option value="XL">XL</option>
                                <option value="XXL">XXL</option>
                            </select>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="card no-border mt-4">
    <div class="card-header">
        Tambahan Lain
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered m-0">
                <tr id="block-issaudarakuliah">
                    <td class="LeftColumnBG">
                        <b id="label-issaudarakuliah">
                            Memiliki saudara di UNMER Malang ?</b>
                    </td>
                    <td class="RightColumnBG">
                        <span id="show">Tidak</span><span id="edit" style="display:none"><input type="radio"
                                name="issaudarakuliah" id="issaudarakuliah_0" value="0" checked=""
                                onchange="cekSaudara()"><label for="_0" onchange="cekSaudara()">Tidak</label>
                            <input type="radio" name="issaudarakuliah" id="issaudarakuliah_-1" value="-1"
                                onchange="cekSaudara()"><label for="_-1" onchange="cekSaudara()">Ya</label>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>