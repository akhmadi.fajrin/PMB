<div class="clearfix">
    <ul class="steps">
        <li class="step step-success">
            <div class="step-content">
                <span class="step-circle">1</span>
                <span class="step-text">Terdaftar</span>
            </div>
        </li>
        <li class="step">
            <div class="step-content">
                <span class="step-circle">2</span>
                <span class="step-text">Berkas diterima</span>
            </div>
        </li>
        <li class="step">
            <div class="step-content">
                <span class="step-circle">3</span>
                <span class="step-text">Lolos seleksi</span>
            </div>
        </li>
        <li class="step">
            <div class="step-content">
                <span class="step-circle">4</span>
                <span class="step-text">Her-registrasi</span>
            </div>
        </li>
    </ul>
</div>