<div class="card no-border">
    <div class="card-header">Asal Sekolah</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered mb-0">
                <tbody>
                    <tr id="block-kodejenissmu">
                        <td class="LeftColumnBG">
                            <b id="label-kodejenissmu">
                                Jenis Sekolah <span id="edit" style="display:none">*</span></b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">Sekolah Lain-Lain</span>
                        </td>
                    </tr>
                    <tr id="block-propinsismu">
                        <td class="LeftColumnBG">
                            <b id="label-propinsismu">
                                Propinsi Sekolah <span id="edit" style="display:none">*</span></b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">BANTEN</span>

                        </td>
                    </tr>
                    <tr id="block-thnmasuksmaasal">
                        <td class="LeftColumnBG">
                            <b id="label-thnmasuksmaasal">
                                Tahun Masuk <span id="edit" style="display:none">*</span></b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">2021</span><span id="edit" style="display:none"><input type="text"
                                    name="thnmasuksmaasal" id="thnmasuksmaasal" value="2021" class="form-control"
                                    maxlength="4" size="5">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-thnlulussmaasal">
                        <td class="LeftColumnBG">
                            <b id="label-thnlulussmaasal">
                                Tahun Lulus <span id="edit" style="display:none">*</span></b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">2021</span><span id="edit" style="display:none"><input type="text"
                                    name="thnlulussmaasal" id="thnlulussmaasal" value="2021" class="form-control"
                                    maxlength="4" size="5">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-alamatsmu">
                        <td class="LeftColumnBG">
                            <b id="label-alamatsmu">
                                Alamat Sekolah <span id="edit" style="display:none">*</span></b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">KP TAMBAK SERANG</span><span id="edit" style="display:none"><input
                                    type="text" name="alamatsmu" id="alamatsmu" value="KP TAMBAK SERANG"
                                    class="form-control" maxlength="60" size="50">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-telpsmu">
                        <td class="LeftColumnBG">
                            <b id="label-telpsmu">
                                Telp Sekolah <span id="edit" style="display:none">*</span></b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">083148556138</span><span id="edit" style="display:none"><input type="text"
                                    name="telpsmu" id="telpsmu" value="083148556138" class="form-control" maxlength="15"
                                    size="15">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-nis">
                        <td class="LeftColumnBG">
                            <b id="label-nis">
                                NIS</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">3025746250</span><span id="edit" style="display:none"><input type="text"
                                    name="nis" id="nis" value="3025746250" class="form-control" maxlength="20"
                                    size="50">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-nemsmu">
                        <td class="LeftColumnBG">
                            <b id="label-nemsmu">
                                NEM Kelulusan</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">60,00</span><span id="edit" style="display:none"><input type="text"
                                    name="nemsmu" id="nemsmu" value="60,00" class="form-control number" maxlength="6"
                                    size="6">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-noijasahsmu">
                        <td class="LeftColumnBG">
                            <b id="label-noijasahsmu">
                                No Ijasah SMU <span id="edit" style="display:none">*</span></b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">DN/PC/0017392</span><span id="edit" style="display:none"><input type="text"
                                    name="noijasahsmu" id="noijasahsmu" value="DN/PC/0017392" class="form-control"
                                    maxlength="20" size="20">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-tglijasahsmu">
                        <td class="LeftColumnBG">
                            <b id="label-tglijasahsmu">
                                Tgl Ijasah SMU</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">3 Mei 2021</span><span id="edit" style="display:none"><input type="text"
                                    name="tglijasahsmu" id="tglijasahsmu" value="03-05-2021" class="form-control"
                                    maxlength="10" size="10"> <img class="btn btn-warning" src="images/cal.png"
                                    id="tglijasahsmu_trg" style="cursor:pointer;" title="Pilih Tgl Ijasah SMU"> &lt;&lt;
                                <i style="font-size:11px">klik untuk
                                    pilih tanggal</i>
                                <script type="text/javascript">
                                Calendar.setup({
                                    inputField: "tglijasahsmu",
                                    ifFormat: "%d-%m-%Y",
                                    button: "tglijasahsmu_trg",
                                    align: "Br",
                                    singleClick: true
                                });
                                </script>

                            </span>
                        </td>
                    </tr>
                    <tr id="block-nrata2uan">
                        <td class="LeftColumnBG">
                            <b id="label-nrata2uan">
                                Nilai Rata-rata UAN</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">29,00</span><span id="edit" style="display:none"><input type="text"
                                    name="nrata2uan" id="nrata2uan" value="29,00" class="form-control number"
                                    maxlength="5" size="3">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>Nilai Raport</b></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-top:0px;">
                            <table border="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td colspan="2" class="text-center" style="padding-right:10px;">
                                            Kelas X
                                        </td>
                                        <td colspan="2" class="text-center" style="padding-right:10px;">
                                            Kelas XI
                                        </td>
                                        <td colspan="2" class="text-center" style="padding-right:10px;">
                                            Kelas
                                            XII</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="padding-right:10px;">Smt. 1
                                        </td>
                                        <td class="text-center" style="padding-right:10px;">Smt. 2
                                        </td>
                                        <td class="text-center" style="padding-right:10px;">Smt. 1
                                        </td>
                                        <td class="text-center" style="padding-right:10px;">Smt. 2
                                        </td>
                                        <td class="text-center" style="padding-right:10px;">Smt. 1
                                        </td>
                                        <td class="text-center" style="padding-right:10px;">Smt. 2
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span id="show">83.00</span><span id="edit" style="display:none"><input
                                                    type="text" name="raport_10_1" id="raport_10_1" value="83.00"
                                                    class="form-control" maxlength="100" size="5"
                                                    style="width:90%;text-align:center;">
                                            </span>
                                        </td>
                                        <td>
                                            <span id="show">81.00</span><span id="edit" style="display:none"><input
                                                    type="text" name="raport_10_2" id="raport_10_2" value="81.00"
                                                    class="form-control" maxlength="100" size="5"
                                                    style="width:90%;text-align:center;">
                                            </span>
                                        </td>
                                        <td>
                                            <span id="show">81.00</span><span id="edit" style="display:none"><input
                                                    type="text" name="raport_11_1" id="raport_11_1" value="81.00"
                                                    class="form-control" maxlength="100" size="5"
                                                    style="width:90%;text-align:center;">
                                            </span>
                                        </td>
                                        <td>
                                            <span id="show">84.00</span><span id="edit" style="display:none"><input
                                                    type="text" name="raport_11_2" id="raport_11_2" value="84.00"
                                                    class="form-control" maxlength="100" size="5"
                                                    style="width:90%;text-align:center;">
                                            </span>
                                        </td>
                                        <td>
                                            <span id="show">83.00</span><span id="edit" style="display:none"><input
                                                    type="text" name="raport_12_1" id="raport_12_1" value="83.00"
                                                    class="form-control" maxlength="100" size="5"
                                                    style="width:90%;text-align:center;">
                                            </span>
                                        </td>
                                        <td>
                                            <span id="show"></span><span id="edit" style="display:none"><input
                                                    type="text" name="raport_12_2" id="raport_12_2" class="form-control"
                                                    maxlength="100" size="5" style="width:90%;text-align:center;">
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Informasi Universitas Asal</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered mb-0">
                <tbody>
                    <tr id="block-mhstransfer">
                        <td class="LeftColumnBG">
                            <b id="label-mhstransfer">
                                Mahasiswa Transfer ?</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">Tidak</span><span id="edit" style="display:none"><input type="radio"
                                    name="mhstransfer" id="mhstransfer_1" value="1"><label for="_1">Ya</label>
                                <input type="radio" name="mhstransfer" id="mhstransfer_0" value="0" checked=""><label
                                    for="_0">Tidak</label>
                            </span>
                        </td>
                    </tr>
                    <tr id="block-ptasal">
                        <td class="LeftColumnBG">
                            <b id="label-ptasal">
                                Universitas Asal</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span><span id="edit" style="display:none"><input type="text" name="ptasal"
                                    id="ptasal" class="form-control" maxlength="50" size="40">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-propinsiptasal">
                        <td class="LeftColumnBG">
                            <b id="label-propinsiptasal">
                                Propinsi universitas </b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftColumnBG">
                            <b>Kota Universitas</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span>
                            <span id="edit" style="display:none"><span id="show"></span><span id="edit"
                                    style="display:none"><select name="kodekotapt" id="kodekotapt" class="form-control">
                                        <option value="">-- Pilih Propinsi terlebih dahulu --
                                        </option>
                                    </select>
                                </span></span>
                        </td>
                    </tr>
                    <tr id="block-ptjurusan">
                        <td class="LeftColumnBG">
                            <b id="label-ptjurusan">
                                Jurusan</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                    name="ptjurusan" id="ptjurusan" class="form-control" maxlength="50" size="40">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-ptthnlulus">
                        <td class="LeftColumnBG">
                            <b id="label-ptthnlulus">
                                Tahun Lulus</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                    name="ptthnlulus" id="ptthnlulus" class="form-control" maxlength="4" size="4">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-ptipk">
                        <td class="LeftColumnBG">
                            <b id="label-ptipk">
                                IPK</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span><span id="edit" style="display:none"><input type="text" name="ptipk"
                                    id="ptipk" class="form-control" maxlength="4" size="4">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-sksasal">
                        <td class="LeftColumnBG">
                            <b id="label-sksasal">
                                SKS</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                    name="sksasal" id="sksasal" class="form-control" maxlength="3" size="4">
                            </span>
                        </td>
                    </tr>
                    <tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card mt-4 no-border">
    <div class="card-header">Pondok Pesantren</div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered mb-0">
                <tbody>
                    <tr id="block-pernahponpes">
                        <td class="LeftColumnBG">
                            <b id="label-pernahponpes">
                                Pernah Belajar di Ponpes ?</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show">Tidak</span><span id="edit" style="display:none"><input type="radio"
                                    name="pernahponpes" id="pernahponpes_1" value="1"><label for="_1">Ya</label>
                                <input type="radio" name="pernahponpes" id="pernahponpes_0" value="0" checked=""><label
                                    for="_0">Tidak</label>
                            </span>
                        </td>
                    </tr>
                    <tr id="block-namaponpes">
                        <td class="LeftColumnBG">
                            <b id="label-namaponpes">
                                Nama Pesantren</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                    name="namaponpes" id="namaponpes" class="form-control" maxlength="50" size="50">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-alamatponpes">
                        <td class="LeftColumnBG">
                            <b id="label-alamatponpes">
                                Alamat Pesantren</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                    name="alamatponpes" id="alamatponpes" class="form-control" maxlength="60" size="50">
                            </span>
                        </td>
                    </tr>
                    <tr id="block-propinsiponpes">
                        <td class="LeftColumnBG">
                            <b id="label-propinsiponpes">
                                Propinsi Pesantren </b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftColumnBG">
                            <b>Kota Pesantren</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span>
                            <span id="edit" style="display:none"><span id="show"></span><span id="edit"
                                    style="display:none"><select name="kodekotaponpes" id="kodekotaponpes"
                                        class="form-control">
                                        <option value="">-- Pilih Propinsi terlebih dahulu --
                                        </option>
                                    </select>
                                </span></span>
                        </td>
                    </tr>
                    <tr id="block-lamaponpes">
                        <td class="LeftColumnBG">
                            <b id="label-lamaponpes">
                                Lama Belajar</b>
                        </td>
                        <td class="RightColumnBG">
                            <span id="show"></span><span id="edit" style="display:none"><input type="text"
                                    name="lamaponpes" id="lamaponpes" class="form-control" maxlength="5" size="5">
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>